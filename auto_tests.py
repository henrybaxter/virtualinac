#!/usr/bin/env python
"""
   VirtuaLinac.auto_tests
   ~~~~~~~~~~~~~~~~~~~~~~~

   Run automated tests of VirtuaLinac

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# auto_tests.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os,sys
import time
import requests
import shutil
import boto3
import vl_api_helper as helper
import tests.fieldsize as fieldsize
from tests.Xml import Xml
import tests.myvarian_photons as myvarian_photons
import tests.myvarian_electrons as myvarian_electrons
import tests.tuning_electrons as tuning_electrons
import tests.tuning_photons as tuning_photons

###############################################################################
# main starts here    
###############################################################################

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--instance_id',
                        help='instance_id to use. Can be i-********, localhost,'
                        ' or spot. Default=spot.',
                        default='spot')
    parser.add_argument('--image_id',
                        help='If requesting an instance, specify the image id.')
    parser.add_argument('--filename', 
                        help='Name of file and directory to store results.',
                        default=None)
    parser.add_argument('--precision', 
                        default=0,
                        help='Level of precision. Lower numbers mean faster. '
                        'Default=0.')
    parser.add_argument('--myvarian_photons',
                        action='store_true',
                        help='Simulate photon beams, starting from phsp '
                        'files.')
    parser.add_argument('--myvarian_electrons',
                        action='store_true',
                        help='Simulate electron beams, starting from phsp '
                        'files.')
    parser.add_argument('--phsp_index',
                        default=0,
                        help='Index of phsp files to use.')
    parser.add_argument('--tuning_electrons',
                        action='store_true',
                        help='Simulate electron beams, starting from incident '
                        'beam.')
    parser.add_argument('--tuning_photons',
                        action='store_true',
                        help='Simulate photon beams, starting from incident '
                        'beam.')
    parser.add_argument('--fieldsize',
                        action='store_true',
                        help='Run the tests for collimated fields.')
    parser.add_argument('--xml',
                        action='store_true',
                        help='Run the tests for XML simulations.')
    parser.add_argument('--noupload',
                        action='store_true',
                        help="Don't upload any files.")
    parser.add_argument('--spot_price',
                        default='1.0',
                        help='Maximum price for spot instance.')
    parser.add_argument('--key_name',
                        help='Name of security key.',
                        default=None)
    parser.add_argument('--security_group',
                        help='Name of security group',
                        default=None)
    parser.add_argument('--instance_type',
                        help='Type of instance. Default c3.8xlarge',
                        default='c3.8xlarge')
    parser.add_argument('--beams',
                        nargs='+',
                        help='Which beams to simulate. 4X, 6FFF, etc. Defaults'
                        ' to all (for the specified group.)')
    parser.add_argument('--keep_alive',
                        action='store_true',
                        help='Keep the instance running when done.')
    parser.add_argument('--testing',
                        action='store_true',
                        help='Run tests under development.')
    args = parser.parse_args()


    instance_id = args.instance_id
    if instance_id == 'spot':
        if args.image_id:
            image_id = args.image_id
        elif os.getenv('VL_IMAGE_ID'):
            image_id = os.getenv('VL_IMAGE_ID')
        else:
            raise Exception('ImageID needs to be specified on command'
                                     ' line or in environment variable '
                                     'VL_IMAGE_ID.')
        if args.key_name:
            key_name = args.key_name
        elif os.getenv('VL_KEY_NAME'):
            key_name = os.getenv('VL_KEY_NAME')
        else:
            raise Exception('Security key needs to be specied on command'
                                 ' line or in environment variable '
                                 'VL_KEY_NAME')

        if args.security_group:
            security_group = args.security_group
        elif os.getenv('VL_SECURITY_GROUP'):
            security_group = os.getenv('VL_SECURITY_GROUP')
        else:
            raise Exception('Security group needs to be '
                'specified on command line or in environment variable '
                'VL_SECURITY_GROUP')
        SpotType = 'persistent' #'one-time'
        SpotPrice = args.spot_price
        LaunchSpecification = {
          'ImageId': image_id,
          'KeyName': key_name,
          'SecurityGroups': [security_group],
          'InstanceType': args.instance_type
        }
        instance_id = helper.request_spot_instances(
            SpotPrice = SpotPrice,
            Type = SpotType,
            LaunchSpecification = LaunchSpecification)

    precision = int(args.precision)

    filename = args.filename
  
    if instance_id != 'localhost':
        ec2 = boto3.resource('ec2')
        instance = ec2.Instance(instance_id)
        url = 'http://' + instance.public_dns_name
    else:
        url = 'http://localhost:8080'
        instance = None
    

    if args.myvarian_photons:
        myvarian_photons.myvarian_photons(url,
                    upload=(not args.noupload), 
                    filename=filename,
                    phsp_index=args.phsp_index,
                    beams=args.beams,
                    precision=precision)

    if args.myvarian_electrons:
        myvarian_electrons.myvarian_electrons(url,
                      upload=(not args.noupload), 
                      filename=filename,
                      phsp_index=args.phsp_index,
                      beams=args.beams,
                      precision=precision)

    if args.tuning_photons:
        tuning_photons.tuning_photons(url,
                      filename=filename,
                      beams=args.beams,
                      precision=precision)

    if args.tuning_electrons:
        tuning_electrons.tuning_electrons(url,
                      filename=filename,
                      beams=args.beams,
                      precision=precision)

    if args.fieldsize:
        fieldsize.fieldsize(url,
                            directory=filename,
                            precision=precision)
    
    if args.xml:
        xml = Xml(url,
                  code_version='10.2.p1', 
                  precision=precision,
                  directory=args.filename)
        xml.all_xml()
    
    if args.testing:
        import tests.penumbra_millenium as penumbra
        penumbra.penumbra(instance,
                    filename=filename,
                    precision=precision)

    if instance:
        if not args.keep_alive:
            helper.terminate_instance(instance)

