#!/usr/bin/env python
"""
   VirtuaLinac.addalldose
   ~~~~~~~~~~~~~~~~~~~~~~~

   Add VirtuaLinac dose distributions

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# addalldose.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.



from __future__ import print_function

def addalldose(average=True, suffix='dose', column=0):
    import sys
    import os
    import math
    import numpy as np

    print('Adding dose files.')

    outputFile = 'sum.dose'
    comments = ''

    l1 = os.listdir('.')
    ls1 = []
    for fn in l1:
        if fn[-len(suffix):]==suffix:
            ls1.append(fn)
    #print ls1
    first = True
    count = 0

    ## first do a sanity check on the files.
    ## get the number of voxels, compare to number of data points in file

    ls = [] 
    for fn in ls1:
        with open(fn, 'r') as f:
            ll = f.readlines()
        commentLines = 0
        binsFound = False
        for l in ll:
            if 'Number of bins' in l:
                zbins = int(l.split()[-1].rstrip(','))
                ybins = int(l.split()[-2].rstrip(','))
                xbins = int(l.split()[-3].rstrip(','))
                binsFound = True
            if l[0] == '#':
                commentLines += 1
        if binsFound:
            if len(ll) - commentLines == xbins * ybins * zbins:
                ls.append(fn)
            else:
                print('Omitting file', fn,'. Incorrect number of data points.')
        else:
            print('Omitting file', fn,'. Incorrect number of data points.')

    print('Adding files:')
    for fn in ls:
        print(' ', fn)

    # a is running mean, q is a value used to calculate the running std dev
    # notation from wikipedia

    n = 0.0

    xvals = []
    for fn in ls:
        with open(fn,'r') as f:
            ll = f.readlines()
        data = []
        n += 1.
        for l in ll:
            if l[0] != '#':
                line = l.split()
                #if len(line) >= 2:
                #   val = float(line[1])
                #else:
                #   val = float(line[0])
                val = float(line[column])
                data.append(val)
                if column > 0 and first:
                    xvals.append(line[0])
            elif first: 
                comments += l
        data = np.array(data)
        #print(int(n), data[0])
        if first:
            a = data
            q = np.zeros(a.size)
            first = False
        else:
            a_old = a
            q_old = q
            a = a_old + (data - a_old)/n
            q = q_old + (data - a_old) * (data - a) 

    # done going through files
    # the mean is a, the std dev of mean is q/(n*(n-1))
    # to report sum, stddev(sum) calculate, std dev of sum = std dev of mean * n

    if n>1:
        stdm = np.sqrt(q/(n*(n-1)))
        ## fails if n=1

    s = ''
    if not average:
        a *= n
        stdm *= n
        s = '# Sum of ' + str(int(n)) + ' data files.\n'
    else:
        s = '# Average of ' + str(int(n)) + ' data files.\n'

    s += comments

    for i in range(a.size):
        if column > 0:
            s += xvals[i] + ' '
        s += str(a[i]) + ' ' +str(stdm[i]) + '\n'
        if i == 0:
            print('First voxel. Average: {0:7.4g}, std.dev: {1:7.4g}'
                  .format(a[i],stdm[i]))

    f = open(outputFile,'w')
    f.write(s)
    f.close()
    print('Averaged {0:.0g} files.'.format(n))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--average',
                        action = 'store_true',
                        default = True,
                        help = 'Record the average of the input files')
    parser.add_argument('--column',
                        help = 'Which column to average',
                        default = 0)
    ##parser.add_argument('--key',
    ##                  help = 'treat first column of data as the key. '
    ##                  'Average values with same key.',
    ##                  action = 'store_true')
    parser.add_argument('--suffix',
                        help = 'Add files that have this suffix.')
                        
    args = parser.parse_args()

    average = args.average
    suffix = 'dose'
    if args.suffix:
        suffix = args.suffix
    column = 0
    if args.column:
        column = int(args.column)

    addalldose(average, suffix=suffix, column=column)

