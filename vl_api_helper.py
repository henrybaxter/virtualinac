"""
   VirtuaLinac.vl_api_helper
   ~~~~~~~~~~~~~~~~~~~~~~~

   Utility functions to use the VirtuaLinac API

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# vl_api_helper.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os
import sys
import time
import shutil
import operator
from bs4 import BeautifulSoup
import requests
import boto3
import addalldose
import plotdose

def request_spot_instances(**kwargs):
    """ request an AWS spot instance """
    client = boto3.client('ec2')

    ## defaults
    SpotPrice = '1.0',
    Type = 'one-time',
    LaunchSpecification = {
      'KeyName': 'worker',
      'SecurityGroups': ['paas-test1-security'],
      'InstanceType': 'c3.4xlarge'
    }

    for k, v in kwargs.iteritems():
        if k == 'Type':
            Type = v
        elif k == 'SpotPrice':
            SpotPrice = v
        elif k == 'LaunchSpecification':
            LaunchSpecification = v

    response = client.request_spot_instances(
        SpotPrice = SpotPrice,
        Type      = Type,
        LaunchSpecification = LaunchSpecification
    )

    request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    print('Spot request id:', request_id)

    print('Waiting for spot instance to be available')
    waiter = client.get_waiter('spot_instance_request_fulfilled')
    waiter.wait(
        SpotInstanceRequestIds = [request_id])

    print('Spot instance started.')
    response = client.describe_spot_instance_requests(
        SpotInstanceRequestIds = [request_id])

    #print(response)

    instance_id = response['SpotInstanceRequests'][0]['InstanceId']
    print('Instance id:', instance_id)

    print('Waiting for instance to start running.')
    waiter = client.get_waiter('instance_running')
    waiter.wait(
        InstanceIds = [instance_id])
    print('Instance running. Wait for 20 seconds.')
    time.sleep(20)

    return instance_id


def terminate_instance(instance):
    """ terminate an instance """
    client = boto3.client('ec2')
    instance_id = instance.instance_id
    
    print('Will now terminate the instance')
    instance.terminate()
    waiter = client.get_waiter('instance_terminated')
    waiter.wait(InstanceIds = [instance_id])
    print('Terminated.')

    ## make sure it is terminated
    response = client.describe_instances(InstanceIds = [instance_id])
    state = response['Reservations'][0]['Instances'][0]['State']['Name']

    print('Double check: Now the instance state is: {0}'.format(state))
    if state != 'terminated':
        print('*************************************')
        print('Not terminated!!!!!!!!!!!!!!!!!!!!!!!')
        print('*************************************')
        return 1
    else:
        return 0


def download(address, filename):
    """ download a file from an instance """
    r = requests.get(address + '/vl_files/' + filename, stream=True)
    if os.path.exists(filename):
        filename = (filename + '.' +
                    str(time.time()).split('.')[0])
    f = open(filename, 'w')
    f.write(r.content)
    f.close()


def print_params(params):
    """ print job parameters """
    print 'Input parameters:'
    print '*************************'
    sorted_params = sorted(params.items(), key=operator.itemgetter(0))
    for k,v in sorted_params:
        print k,v
    print '*************************'


def get_file(upload_string, filename, params=None, timeout=600, 
             directory=None):
    """ download a file, using GET """
    ## if directory is specified, move file to that directory
    ## can specify GET params with keyword params
    #print params
    try:
        resp1 = requests.get(upload_string, params=params, 
                             stream=True, timeout=timeout)
    except requests.Timeout:
        pass
    else:
        with open(filename, 'wb') as f:
            for chunk in resp1.iter_content(chunk_size=1024):
                f.write(chunk)
    if directory is not None:
        move_safely(filename, directory)

def _wait_for_jobs(address, sleeptime=30, firsttime=False, wait=True):
    """Check the number of running jobs; if there are running jobs, wait."""
    running_jobs = -1
    while running_jobs != 0:
        if not firsttime:
            time.sleep(sleeptime)
        firsttime = False
        #try:
        if (1):
            try:
                resp = requests.get(address + '/usage')
            except:# ConnectionError:
                print('Connection error!')
            else:
                response = ''.join(resp)
                for line in response.split('\n'):
                    if 'Number' in line:
                        running_jobs = int(line.split()[-2])
                sys.stdout.write(time.asctime() + ' ' + address
                                    #' file name: ' + filename
                                    + ': jobs = '
                                    + str(running_jobs) + '\n')
                if running_jobs < 0:
                    sys.stdout.write('Error getting number of jobs!\n')
                sys.stdout.flush()
        else:
            pass
        if not wait:
            return running_jobs
    print 'Done!'
    return running_jobs

#def _wait_for_jobs(instance, sleeptime=30, firsttime=False, wait=True):
#    """Check the number of running jobs; if there are running jobs, wait."""
#    ## first check if instance isn't terminated
#    print instance.state['Name']
#    restarted = False
#    running_jobs = 2
#    address = "http://" + instance.public_dns_name
#    print instance.instance_lifecycle
#    while running_jobs != 0:
#        #while instance.state['Code'] != 16:
#        #    print time.asctime(), ' Not running! Wait for 5 minutes.'
#        #    time.wait(300)
#        #    restarted = True
#        #if restarted:
#        #    return -1
#
#        if not firsttime:
#            time.sleep(sleeptime)
#        firsttime = False
#        #try:
#        if (1):
#            try:
#                resp = requests.get(address + '/usage')
#            except:# ConnectionError:
#                print('Connection error!')
#                time.sleep(300)
#                return -3
#            else:
#                response = ''.join(resp)
#                for line in response.split('\n'):
#                    if 'Number' in line:
#                        running_jobs = int(line.split()[-2])
#                sys.stdout.write(time.asctime() + ' ' + address
#                                    #' file name: ' + filename
#                                    + ': jobs = '
#                                    + str(running_jobs) + '\n')
#                if running_jobs < 0:
#                    sys.stdout.write('Error getting number of jobs!\n')
#                sys.stdout.flush()
#        else:
#            pass
#        if not wait:
#            return running_jobs
#    print 'Done!'
#    return running_jobs


def get_home(address):
    """ get the options from the home page """

    def gettag(name):
        """ get the options for each tag """
        result = {}
        for l in links:
            if l['id'] == name:
                opts = l.find_all('option')
                for opt in opts:
                    val = opt['value']
                    txt = opt.string
                    result[txt] = val
        options[name] = result
        return result

    #TODO
    #def getradio(name):
    #    result = {}
    #    for l in links:
    #        if l['name'] == 'beam_type':
    #            print 'BEAM TYPE'
    #            print l
    #            value = l['value']
    #            nxt = l.next_sibling
    #            print 'nxt:', nxt

    options = {}
    if address[:7] != 'http://':
        address = 'http://' + address
    print 'Address:', address
    resp = requests.get(address)
    if resp.status_code == requests.codes.ok:
        soup = BeautifulSoup(resp.text, 'html.parser')
        links = soup.find_all('select')
        beam_types     = gettag('beam_type')
        code_versions  = gettag('code_version')
        physics_lists  = gettag('physics_list')
        targets        = gettag('target')
        flattening_filters = gettag('flattening_filter')
        phsp_positions = gettag('phsp_position')
        foil1          = gettag('foil1')
        foil2          = gettag('foil2')
        applicator     = gettag('applicator')

        #links = soup.find_all('input')
        #beam_types = getradio('beam_type')

    #options['code_versions'] = code_versions
    #options['targets']       = targets
    #options['phsp_positions'] = phsp_positions
    #options['physics_lists'] = physics_lists
    #options['beam_types']    = beam_types

    return options

##def get_file_index(address, filename):
##    """given a file name to be used as input, determine the index
##    to request."""
##    suffix = filename.split('.')[-1]
##    files = []
##    resp = requests.get(address + '/phspfiles')
##    if resp.status_code == requests.codes.ok:
##        soup = BeautifulSoup(resp.text, 'html.parser')
##        links = soup.find_all('a')
##        for link in links:
##            href = link['href']
##            if 'vl_files' in href:
##                spl = href.split('/')
##                if len(spl) > 2:
##                    fn = href.split('/')[2]
##                    if fn.split('.')[-1] == suffix:
##                        files.append(fn)
##    files.sort()
##    for i, f in enumerate(files):
##        if f == filename:
##            return i
##    ## file is not found!
##    print('File ', filename, ' not found!')
##    exit()
##    return -1

def addresults(filename, beam, zplot=4, msmt_file_x=None, msmt_file_z=None):
    """ sum individual dose files, place in directory, create png """
    cwd = os.getcwd()
    newdir = os.path.join(filename,beam)
    if not os.path.exists(newdir):
        os.mkdir(newdir)
    ff = [f for f in os.listdir('.') if filename in f and beam in f]
    for f in ff:
        shutil.move(f, newdir)
    os.chdir(newdir)
    addalldose.addalldose()
    newfn = beam + '_' + filename + '.dose' 
    shutil.move('sum.dose', newfn)
    plotdose.plotdose(newfn, plotdir='x', xplot=-1, yplot=-1, zplot=zplot, 
                      average=4, savefig=True, msmt_file=msmt_file_x)
    plotdose.plotdose(newfn, plotdir='z', xplot=-1, yplot=-1, zplot=-1, 
                      average=4, savefig=True, msmt_file=msmt_file_z)
    os.chdir(cwd)

def move_safely(filename, directory):
    if not os.path.exists(directory):
        os.mkdir(directory)
    if not os.path.isdir(directory):
        directory = directory + '_' + str(int(time.time()))
        os.mkdir(directory)
    if os.path.exists(os.path.join(directory, filename)):
        new_filename = filename + '_' + str(int(time.time()))
        shutil.move(filename, new_filename)
        filename = new_filename
    try:
        shutil.move(filename, directory)
    except:
        print "Couldn't move", filename, "to", directory, "."
