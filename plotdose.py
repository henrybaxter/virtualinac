#!/usr/bin/env python
"""
   VirtuaLinac.plotdose
   ~~~~~~~~~~~~~~~~~~~~~~~

   Plot dose from a VirtuaLinac dose distribution file

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey@varian dot com)
"""

# plotdose.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# Expect that file is a (ascii) list of doses for different voxels, one per line.
# Comments at top of dose file start with #.
# One line gives number of bins: #Number of bins in x, y, z: XX, YY, ZZ
# One line gives phantom size:   #Phantom size [mm]: XX, YY, ZZ
# Phantom size assumed to be in mm in this program.
# Index is not recorded but # of bins and phantom size in header.
# Z index changes fastest, then Y, then X

import numpy
import os
import sys
import math
import matplotlib.pyplot as plt

def plotdose(fn,               # filename
             xplot, yplot, zplot, # which bin to plot for the three axes
             plotdir,          # axis to plot: x, y, z, d(iagonal), all 
             average=1,        # number of bins to average
             average_z=1,      # number of bins along z to average
             average_along=1,  # number of bins to average in direction of plot
             average_central=True, # if we are plotting the central bin with 
                               # even number of bins, average two
             showGraph=True,   # create a matplotlib figure
             savefig=False,    # save a png file
             normalize=True,   # normalize the graph to 100%
             rescale = 1.0,    # multiply results by this factor
             msmt_file=None,   # compare to results in this file
             dimension=1,      # plot dimension: 1 for a line plot, 
                               # 2 for colourmap
             output_file=True, # write out a data file
             colorbar_label=None,   # label for colorbar, on 2D plots
             aspect='auto',    # aspect ratio for colormap plots
             colormap='viridis', # matplotlib cmap
             value_to_read=0,  # the index of value to read in input file line
             verbosity=0):

    errors = ''

    if showGraph:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

    with open(fn, 'r') as f:
        ll = f.readlines()

    ### Read the header ###
    #get number of voxels and phantom size in each direction
    nx = ny = nz = 0
    psizex = psizey = psizez = 0
    i = 0
    voxel_size = False
    while ll[i][0] == '#':
        if 'Number of bins in' in ll[i] or 'Number of voxels' in ll[i]:
            nz = int(ll[i].split()[-1])
            ny = int(ll[i].split()[-2].rstrip(','))
            nx = int(ll[i].split()[-3].rstrip(','))
        elif 'Phantom size' in ll[i]:
            l = ll[i].rstrip('\nm ')  ## remove mm
            psizez = float(l.split()[-1])
            psizey = float(l.split()[-2].rstrip(','))
            psizex = float(l.split()[-3].rstrip(','))
        elif 'Voxel size' in ll[i]:
            voxel_size = True
            l = ll[i].rstrip('\nm ')  ## remove mm
            psizez = float(l.split()[-1])
            psizey = float(l.split()[-2].rstrip(','))
            psizex = float(l.split()[-3].rstrip(','))
        i += 1

    ## phantom file specifies voxel size, dose file specifies phantom size
    ## convert to phantom size
    if voxel_size:
        psizex = psizex*float(nx)
        psizey = psizey*float(ny)
        psizez = psizez*float(nz)

    print 'phantom size [mm]:',psizex, psizey, psizez
    #exit()

    dose   = []
    uncert = []
 
    # default plot indices
    if nx == 1:
        xplot = 0
    elif xplot < 0:
        xplot = nx/2
    elif xplot > nx - 1:
        #xplot = nx - 1
        if plotdir == 'y' or plotdir == 'z':
            errors += 'X value out of range.\n'

    if ny == 1:
        yplot = 0
    elif yplot < 0:
        yplot = ny/2
    elif yplot > ny - 1:
        yplot = ny - 1
        if plotdir == 'x' or plotdir == 'z':
            errors += 'Y value out of range.\n'

    if nz == 1:
        zplot = 0
    elif zplot < 0:
        zplot = nz/2
    elif zplot > nz - 1:
        zplot = nz -1
        if plotdir == 'x' or plotdir == 'y':
            errors += 'Z value out of range.\n'

    print 'Voxels to plot. xplot:', xplot, 'yplot:', yplot, 'zplot:', zplot
    print 'Number of voxels in phantom.  nx:', nx, 'ny:', ny, 'nz:', nz
    outfn = '%s%s.%g_%g_%g.plt' % (fn[:fn.rfind('.')+1], 
                                plotdir, xplot, yplot, zplot)
  
    #read everything
    for l in ll:
        if l[0] != '#':
            s = l.split()
            dose.append(float(s[int(value_to_read)]))
            #if len(s) > 1:
            #   uncert.append(float(s[1]))

    dose   = numpy.array(dose)
    uncert = numpy.array(uncert)
    dose.resize(nx,ny,nz)
    uncert.resize(nx,ny,nz)

    ## TODO: sanity checks on navg etc

    count = 0.
    if average == 0:
        errors += "Can't average over 0 voxels."

    avg_min, avg_max, errors1 = _get_avg_minmax(
        average, plotdir, dimension, dose, xplot, yplot, zplot, 
        average_central)
    errors += errors1

    xp = []
    yp = []

    if errors:
        ax.plot(xp,yp)
        fig.text(0.2,0.8,
            errors)
        return fig
    if dimension == 1:
        if plotdir == 'z':
            y1 = numpy.zeros(nz)
            y2 = numpy.zeros(nz)
            for i in range(avg_min, avg_max): 
                for j in range(avg_min, avg_max):
                    y1 +=   dose[xplot+i, yplot+j, :]
                    y2 += uncert[xplot+i, yplot+j, :]
                    count += 1.
            if count < 0.5:  # i.e. == 0.0
                count = 1.
            yp = y1/count
            yu = y2/count/math.sqrt(count)
            xp = [(i+0.5) * psizez/float(nz) for i in range(nz)]
            xp = numpy.array(xp)
            if showGraph:
                ax.set_xlabel('Depth [mm]')
                ax.set_ylabel('Dose [Gy/incident particle]')

        elif plotdir == 'x':
            y1 = numpy.zeros(nx)
            y2 = numpy.zeros(nx)
            ## TODO avg_z ## range, to keep same indices
            for i in range(avg_min, avg_max): 
                for j in range(0, 1): 
                    y1 +=   dose[:, yplot+i, zplot+j]
                    y2 += uncert[:, yplot+i, zplot+j]
                    count += 1.
            if count < 0.5:  # i.e. == 0.0
                count = 1.
            yp = y1/count
            yu = y2/count/math.sqrt(count)
            xp = [(i+0.5 - float(nx)/2.) * psizex/float(nx) for i in range(nx)]
            xp = numpy.array(xp)
            if showGraph:
                ax.set_xlabel('Crossplane position [mm]')
                ax.set_ylabel('Dose [Gy/incident particle]')

        elif plotdir == 'y':
            y1 = numpy.zeros(ny)
            y2 = numpy.zeros(ny)
            for i in range(avg_min, avg_max): 
                for j in range(0, 1): 
                    y1 +=   dose[xplot+i, :, zplot+j]
                    y2 += uncert[xplot+i, :, zplot+j]
                    count += 1.
            if count < 0.5:  # i.e. == 0.0
                count = 1.
            yp = y1/count
            yu = y2/count/math.sqrt(count)
            xp = [(i+0.5 - float(ny)/2.) * psizey/float(ny) for i in range(ny)]
            xp = numpy.array(xp)
            if showGraph:
                ax.set_xlabel('Inplane position [mm]')
                ax.set_ylabel('Dose [Gy/incident particle]')

        elif plotdir == 'd':
            # no averaging!
            # TODO don't make this assumption
            # assume phantom has same # of x and y voxels
            y1 = []
            y2 = []
            for i in range(nx):
                y1.append(  dose[i, i, zplot])  
                y2.append(uncert[i, i, zplot])  
            xp = [(i+0.5 - float(ny)/2.) * psizey/float(ny) for i in range(ny)]
            xp = numpy.array(xp)
            xp *= math.sqrt(2.)
            yp = numpy.array(y1)
            yu = numpy.array(y2)
            if showGraph:
                ax.set_xlabel('Diagonal position [mm]')
                ax.set_ylabel('Dose [Gy/incident particle]')

        ## average along plot direction:
        if average_along > 1:
            ## TODO uncertainty
            xp, yp, yu = _avg_along_plot(xp, yp, yu, average_along)

        ## normalize to 100%
        if normalize:
            _normalize(xp, yp, yu, plotdir)

        ## rescale according to command line parameter
        yp *= rescale

        # write out a data file
        if output_file:
            outs = ''
            for i in range(len(xp)):
                outs += str(xp[i]) + ' ' + str(yp[i]) + ' ' + str(yu[i]) + '\n'
            f = open(outfn, 'w')
            f.write(outs)
            f.close()

        ### Read the measured data
        if msmt_file is not None:
            with open(msmt_file,'r') as f:
                ll = f.readlines()
            xmsmt = []
            ymsmt = []
            for l in ll:
                if l[0] != '#':
                    lsplit = l.split()
                    xmsmt.append(float(lsplit[0]))
                    ymsmt.append(float(lsplit[1]))
     
        # plot
        if showGraph:
            #print len(xp),len(yp),len(yu)
            #ax.errorbar(xp, yp, yerr=yu, fmt='o-', label='Simulation')
            ax.plot(xp, yp, 'o-', label='Simulation')
            if 0: ## check symmetry
                ax.errorbar(-xp, yp, yerr=yu, fmt='o-', label='Mirror')
            if msmt_file is not None:
                ax.plot(xmsmt, ymsmt, 'rx-', label='Measurement')
                xdiffs, ydiffs = _get_percent_diffs(xp, yp, xmsmt, ymsmt)
                ax2 = ax.twinx()
                ax2.plot(xdiffs, ydiffs, 'g+-', label='Difference')
                ax2.set_ylabel('Difference [%]')
            ax.grid()
            lines, labels = ax.get_legend_handles_labels()
            if msmt_file is not None:
                lines2, labels2 = ax2.get_legend_handles_labels()
            if plotdir == 'z':
                loc = 1
            else:
                loc = 4
            if msmt_file is not None:
                ax2.legend(lines + lines2, 
                           labels + labels2,
                           loc=loc, 
                           prop={'size':8},
                           numpoints=1)
            else:
                ax.legend(lines,
                          labels,
                          loc=loc,
                          prop={'size':8},
                          numpoints=1)  

            tempfn = fn[27:]
            ax.set_title('Dose distribution: ' + tempfn)
            if savefig:
                outfn = 'plot_' + plotdir + '.png'
                plt.savefig(outfn,bbox_inches=0)

            return fig

    else: # dimension == 2
        count = 0.
        if plotdir == 'z':
            film = numpy.zeros([nx,ny])
            for i in range(avg_min, avg_max):
                film += dose[:, :, zplot+i]
                count += 1.
            xmin = -psizex/2.
            xmax =  psizex/2.
            ymin = -psizey/2.
            ymax =  psizey/2.
            ax.set_xlabel('Off-axis position (Crossplane) [mm]')
            ax.set_ylabel('Off-axis position (Inplane) [mm]')
        elif plotdir == 'x':
            film = numpy.zeros([ny,nz])
            for i in range(avg_min,avg_max):
                film += dose[xplot+i, :, :]
                count += 1.
            xmin = -psizey/2.
            xmax =  psizey/2.
            ymin =  psizez
            ymax =  0
            ax.set_xlabel('Off-axis position (Inplane) [mm]')
            ax.set_ylabel('Depth [mm]')
        elif plotdir == 'y':
            film = numpy.zeros([nx, nz])
            for i in range(avg_min, avg_max):
                film += dose[:, yplot+i, :]
                count += 1.
            xmin = -psizex/2.
            xmax =  psizex/2.
            ymin =  psizez
            ymax =  0
            ax.set_xlabel('Off-axis position (Crossplane) [mm]')
            ax.set_ylabel('Depth [mm]')
        film = film.T

        if count > 0.5:  # i.e. != 0.0
            film /= count
        origin = 'upper'
        if plotdir == 'z':
            origin = 'lower'
        if fn.split('.')[-1] == 'phantom':
            label = 'Material index'
        else:
            label = 'Dose [Gy/incident particle]'
        try:
            cmap = plt.get_cmap(colormap)
        except:
            cmap = plt.get_cmap('viridis')
        #if fn.split('.')[-1] == 'phantom':
        #    cmap = plt.get_cmap('gray')
        if aspect.lower() != 'auto' and aspect.lower() != 'equal':
            try:
                aspect = float(aspect)
            except:
                aspect = 'auto'
        cax = ax.imshow(film,
                        interpolation='None',
                        aspect=aspect, 
                        origin=origin,
                        extent=[xmin,xmax,ymin,ymax],
                        cmap=cmap)
        if colorbar_label:
            label = colorbar_label
        cbar = fig.colorbar(cax, label=label)

        if savefig:
            outfn = 'test.png'
            plt.savefig(outfn,bbox_inches=0)
        return fig

def _get_avg_minmax(avg, dir, dim, dosedist, x, y, z, avg_central): 
    """return min, max of range to average over, such that range does not
    exceed size of dose array. 
    avg = number of voxels to average
    dir = direction, one of x,y,z
    dim = dimension of plot (1 or 2)
    dosedist = the three dimensional dose array
    x,y,z = indices to plot"""
   
    # intentionally using integer division (Python2)
    lo = -(avg/2)
    lo_1 = lo
    hi = (avg + 1)/2
    hi_1 = hi
    errors = ''
    sizex = numpy.shape(dosedist)[0] 
    sizey = numpy.shape(dosedist)[1]  
    sizez = numpy.shape(dosedist)[2]
    if dim == 1:
        if dir == 'x':
            if avg_central and not sizey % 2:
                lo -= 1
            if y + lo < 0:
                lo = -y
            if y + hi > sizey:
                hi = sizey - y - 1
        if dir == 'y':
            if avg_central and not sizex % 2:
                lo -= 1
            if x + lo < 0:
                lo = -x
            if x + hi > sizex:
                hi = sizex - x - 1
        if dir == 'z':
            ## choose sizex rather than sizey arbitrarily
            if avg_central and not sizex % 2:
                lo -= 1
            if (x + lo < 0) or (y + lo < 0):
                lo = min(x, y)
            if x + hi > sizex:
                hi = sizex - x - 1
            if y + hi > sizey:
                hi = sizey - y - 1
    elif dim == 2:
        if dir == 'x':
            if x + lo < 0:
                lo = -x
            if x + hi >= sizex:
                hi = sizex - x -1
        elif dir == 'y':
            if y + lo < 0:
                lo = -y
            if y + hi >= sizey:
                hi = sizey - y -1
        elif dir == 'z':
            if z + lo < 0:
                lo = -z
            if z + hi >= sizez:
                hi = sizez - z - 1
    #if (lo != lo_1) or (hi != hi_1):
    #    errors = 'Attempting to average over more voxels than in array.'
    return lo, hi, errors


def _normalize(x, y, yerr, dir):
    """Rescale the plot such that values near x=0 (for profiles)
    or near ymax (for pdd) are 100%
    """
    if dir != 'z':
        #sum = 0.
        #count = 0.
        ## find the two points either side of x=0
        ## xvalues are in different units for phsp/dose
        found = False
        xtmp1 = -999
        xtmp2 =  999
        ytmp1 = 0
        ytmp2 = 0
        for xval, yval in zip(x,y):
            if 0 > xval > xtmp1:
                xtmp1 = xval
                ytmp1 = yval
            if 0 < xval < xtmp2:
                xtmp2 = xval
                ytmp2 = yval
        avg = (ytmp1 + ytmp2)/2.
        y /= (avg/100.)
        yerr /= (avg/100.)

    else:  # dir == z
        # average over ymax plus one previous point
        i_max = y.argmax()
        print 'i_max:', i_max
        count = 1.
        sum = numpy.amax(y)
        if i_max > 0:
           count += 1.
           sum += y[i_max-1]
        #if i_max < len(y) - 1:
        #    count += 1.
        #    sum += y[i_max+1]
        max = sum/count
        y /= (max/100.)
        yerr /= (max/100.)

def _avg_along_plot(x, y, yerr, number_to_average):
    """Average the plot along the plot direction, reducing the number of
    points"""
    xnew = []
    ynew = []
    yerrnew = []
    i = 0
    while i < len(x):
        x1 = 0.
        y1 = 0.
        yerr1 = 0.
        count = 0.
        for j in range(i, min(i+number_to_average, len(x))):
            x1 += x[j]
            y1 += y[j]
            yerr1 += yerr[i]
            count += 1.
            i += 1
        if count < 0.5:
            count = 1.0
        xnew.append(x1/count)
        ynew.append(y1/count)
        yerrnew.append(yerr1/count/math.sqrt(count))
    return numpy.array(xnew), numpy.array(ynew), numpy.array(yerrnew)
 
def _get_percent_diffs(x1,y1,x2,y2):
    ## find the % differences
    # for each mc point, find all msmt points for which the closest mc point 
    # is the one under consideration
    # exception is at the last mc point; limit to 2 mm deeper
    """ x1/y1 assumed to be sim (fewer data points) """
    # do the first and last points separately, to limit the range
    xdiffs = []
    ydiffs = []
    for i in range(len(x1)):
        diff = 0
        count = 0
        for j in range(len(x2)):
            xdelta = abs(x2[j] - x1[i])
            use_this_point = True
            if (i == 0) or (i == len(x1) - 1):
                if abs(x2[j] - x1[i]) > 3:
                    use_this_point = False
            for k in range(len(x1)):
                if abs(x2[j] - x1[k]) < xdelta:
                    use_this_point = False
                    break
            if use_this_point:
                diff += y1[i] - y2[j]
                count += 1
        if count > 0:
            ydiffs.append(float(diff)/float(count))
            xdiffs.append(x1[i])
        else:
            pass
    return xdiffs, ydiffs



def main():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('filename')
    parser.add_argument('plotDirection', 
            help='Direction to plot in: x, y, or z.')
    parser.add_argument('--average', 
            default=1,
            help='number of bins to average on either side')
    parser.add_argument('--average_z', 
            default=1,
            help='number of bins to average in z direction, for profiles')
    parser.add_argument('--average_along', 
            default=1,
            help='number of bins to average along the direction of the plot.')
    parser.add_argument('--average_central', 
            action='store_true',
            default=True,
            help='when plotting central voxel with an even number of voxels, '
            'plot average both central voxels (or all 4, for pdd)')
    parser.add_argument('--zplot', 
            default=6,
            help='Z voxel to plot. Defaults to 6.')
    parser.add_argument('--xplot', 
            default=-1,
            help='X voxel to plot. Defaults to central value.')
    parser.add_argument('--yplot', 
            default=-1,
            help='Y voxel to plot. Defaults to central value.')
    parser.add_argument('--normalize', 
            action='store_true',
            help='Normalize to the plot to 100 percent.')
    parser.add_argument('--rescale',
            default='1.0',
            help='Multiply simulation data by this amount (after '
            'normalizing, if selected.)')
    parser.add_argument('--nograph', 
            action='store_true',
            help="Don't show the plot. Only write the data file.")
    parser.add_argument('--savefig',
            action='store_true',
            help='Save a png file of the plot.')
    parser.add_argument('--measurement_file',
            help='Name of file containing measured data to compare to.')
    parser.add_argument('--dimension',
            default='1',
            help='Create a 1 or 2 dimensional plot')
    parser.add_argument('--colorbar_label',
            help='Label to give to colorbar for colormaps',
            default=None)
    parser.add_argument('--aspect',
            default='auto',
            help='set aspect ratio of colormap plot. Allowed values are equal,'
            'auto or a ratio (float). Default is auto.')
    parser.add_argument('--colormap',
            default='viridis',
            help='matplotlib colormap to use for 2D (color) plots. Default '
            ' is viridis.')
    parser.add_argument('--value_to_read',
            default=0,
            help='if there is more than one value on each line in the input '
            'file, use this one. Default = 0 (first).')

    args = parser.parse_args()

    fn = args.filename
    plotdir = args.plotDirection
    zplot = int(args.zplot)
    xplot = int(args.xplot)
    yplot = int(args.yplot)

    normalize = args.normalize
    if args.measurement_file is not None:
        normalize = True
    rescale = float(args.rescale)   

    if fn == 'all':
        files = [f for f in os.listdir('.') if f[-5:] == '.dose']
    else:
        files = [fn]

    if plotdir == 'all':
        directions = ['z','x','y']
    else:
        directions = plotdir

    print 'average central:', args.average_central

    for file in files:
        for direction in directions:
            fig = plotdose(file,xplot,yplot,zplot,direction,
                     average = int(args.average),
                     average_z = int(args.average_z),
                     average_along = int(args.average_along),
                     average_central = args.average_central,
                     normalize = normalize,
                     rescale = rescale,
                     msmt_file = args.measurement_file,
                     dimension = int(args.dimension),
                     showGraph = (not args.nograph),
                     savefig   = args.savefig,
                     colorbar_label = args.colorbar_label,
                     aspect = args.aspect,
                     colormap = args.colormap,
                     value_to_read = int(args.value_to_read))

    if not args.nograph:
        plt.show()

if __name__ == '__main__':
    status = main()
    sys.exit(status)

