"""
   VirtuaLinac.myvarian_electrons_config
   ~~~~~~~~~~~~~~~~~~~~~~~

   Configuration data for tuning electron beams tests

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# tuning_electrons_config.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os

def config():

    datadir = os.getenv('VL_TEST_DATADIR')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    config = {
        'energy' : {
            '6E'  :  6.84,
            '9E'  :  9.78,
            '12E' : 13.18,
            '15E' : 16.27,
            '16E' : 17.28,
            '18E' : 19.83,
            '20E' : 21.8 ,
            '22E' : 23.5
        },

        'energy_spread' : {
            '6E'  : 0.6,
            '9E'  : 0.6,
            '12E' : 0.7,
            '15E' : 0.9,
            '16E' : 0.6,
            '18E' : 0.8,
            '20E' : 1.0,
            '22E' : 0.6
        },

        'spot_size_x' : 0.7,
        'spot_size_y' : 0.7,

        'ic_metal_thickness_factor' : 0.64, ##  0.8,

        'foil1' : {
            '6E'  : 0,
            '9E'  : 1,
            '12E' : 2,
            '15E' : 3,
            '16E' : 4,
            '18E' : 5,
            '20E' : 6,
            '22E' : 7
        },
  
        'foil2' : {
            '6E'  : 0,
            '9E'  : 1,
            '12E' : 2,
            '15E' : 3,
            '16E' : 4,
            '18E' : 5,
            '20E' : 6,
            '22E' : 7
        },



        'foil1_thickness_factor' : {
            '6E'  : 1.07,
            '9E'  : 1.08,
            '12E' : 1.02,
            '15E' : 1.05,
            '16E' : 1.02,
            '18E' : 1.02,
            '20E' : 1.02,
            '22E' : 1.05
        },

        'data_dir' : datadir,

        'pdd' : {
            '6E'  : '6E_Z.dat',
            '9E'  : '9E_Z.dat',
            '12E' : '12E_Z.dat',
            '15E' : '15E_Z.dat',
            '16E' : '16E_Z.dat',
            '18E' : '18E_Z.dat',
            '20E' : '20E_Z.dat',
            '22E' : '22E_Z.dat'
        },

        'prf' : {
            '6E'  : '6E_X_dmax.dat',
            '9E'  : '9E_X_dmax.dat',
            '12E' : '12E_X_dmax.dat',
            '15E' : '15E_X_dmax.dat',
            '16E' : '16E_X_dmax.dat',
            '18E' : '18E_X_dmax.dat',
            '20E' : '20E_X_dmax.dat',
            '22E' : '22E_X_dmax.dat'
        },
        
        'airprf' : {   # profiles in air at 5 cm above iso
            '6E'  : '6E_X_air.dat',
            '9E'  : '9E_X_air.dat',
            '12E' : '12E_X_air.dat',
            '15E' : '15E_X_air.dat',
            '16E' : '16E_X_air.dat',
            '18E' : '18E_X_air.dat',
            '20E' : '20E_X_air.dat',
            '22E' : '22E_X_air.dat'
        },

        'voxel_z' : {
            '6E'  : 6,
            '9E'  : 9,
            '12E' : 13,
            '15E' : 16,
            '16E' : 16,
            '18E' : 16,
            '20E' : 15,
            '22E' : 14
        },

        'phantom_z' : {
            '6E'  : 50,
            '9E'  : 50,
            '12E' : 50,
            '15E' : 60,
            '16E' : 60,
            '18E' : 70,
            '20E' : 70,
            '22E' : 75
        }
    }

    return config


def params():
    params = {}
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['beam_type']    = 1
    params['range_cut'] = 10
    params['jaw_position_y1'] = -20
    params['jaw_position_y2'] =  20
    params['jaw_position_x1'] = -20
    params['jaw_position_x2'] =  20
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 500
    params['phantom_size_y'] = 500
    params['phantom_voxels_x'] = 100
    params['phantom_voxels_y'] = 100
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['brem_splitting'] = None
    params['splitting_factor'] = None

    return params
