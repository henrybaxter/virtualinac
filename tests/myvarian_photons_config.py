"""
   VirtuaLinac.myvarian_photons_config
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Configuration parameters for simulations starting from MyVarian photon phsp

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# myvarian_photons_config.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os

def config(phsp_index=0):
    if int(phsp_index) < 0 or int(phsp_index) > 55:
        print 'Phase space index', phsp_index, 'out of range!'
        exit()
        return 1
    phsp_index = '{0:02}'.format(int(phsp_index))
    config = {
        'phsp_dir' : os.getenv('VL_TEST_PHSPDIR'),
        'source_phsp' : {
            '4X'    : 'TrueBeam_4X_v2_'    + phsp_index + '.IAEAphsp',
            '6X'    : 'TrueBeam_6X_v2_'    + phsp_index + '.IAEAphsp',
            '6FFF'  : 'TrueBeam_6FFF_v2_'  + phsp_index + '.IAEAphsp',
            '8X'    : 'TrueBeam_8X_v2_'    + phsp_index + '.IAEAphsp',
            '10X'   : 'TrueBeam_10X_v2_'   + phsp_index + '.IAEAphsp',
            '10FFF' : 'TrueBeam_10FFF_v2_' + phsp_index + '.IAEAphsp',
            '15X'   : 'TrueBeam_15X_v2_'   + phsp_index + '.IAEAphsp'
        },
       
        'data_dir' : os.getenv('VL_TEST_DATADIR'),

        'pdd' : {
            '4X'    : '4X_Z_40x40.dat',
            '6X'    : '6X_Z_40x40.dat',
            '6FFF'  : '6FFF_Z_40x40.dat',
            '8X'    : '8X_Z_40x40.dat',
            '10X'   : '10X_Z_40x40.dat',
            '10FFF' : '10FFF_Z_40x40.dat',
            '15X'   : '15X_Z_40x40.dat'
        },

        'prf' : {
            '4X'    : '4X_X_1p2_40x40.dat',
            '6X'    : '6X_X_1p5_40x40.dat',
            '6FFF'  : '6FFF_X_1p5_40x40.dat',
            '8X'    : '8X_X_2p0_40x40.dat',
            '10X'   : '10X_X_2p4_40x40.dat',
            '10FFF' : '10FFF_X_2p4_40x40.dat',
            '15X'   : '15X_X_3p0_40x40.dat'
        },
        'voxel_z' : {
            '4X'    : 3,
            '6X'    : 3,
            '6FFF'  : 3,
            '8X'    : 4,
            '10X'   : 6,
            '10FFF' : 6,
            '15X'   : 7
        }
    }

    return config


def params():
    params = {}
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['beam_type']    = 0
    params['range_cut'] = 10
    params['phsp_input_bool'] = True
    params['jaw_position_y1'] = -20
    params['jaw_position_y2'] =  20
    params['jaw_position_x1'] = -20
    params['jaw_position_x2'] =  20
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 500
    params['phantom_size_y'] = 500
    params['phantom_size_z'] = 400
    params['phantom_voxels_x'] = 100
    params['phantom_voxels_y'] = 100
    params['phantom_voxels_z'] = 80
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -20
    params['brem_splitting'] = None
    params['splitting_factor'] = 10

    return params
