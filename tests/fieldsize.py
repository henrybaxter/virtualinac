"""
   VirtuaLinac.field_size
   ~~~~~~~~~~~~~~~~~~~~~~~

   test VirtuaLinac dose distributions for various field sizes

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# field_size.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os
import requests
import vl_api_helper as helper

def dose_20x20(url, 
               options,
               precision=0,
               code_version='10.2.p1',
               directory='smallfield'):
    """ Calculate dose to water for 20x20 6FFF beam; compare to measurement. """

    datadir = os.path.join(os.getenv('VL_TEST_DATADIR'), '6FFF')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    #upload measurement files
    up_url = url + '/upload'
    files = {'file': open(os.path.join(datadir, '6FFF_Z_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_1p5_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_5_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_10_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_20_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_30_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_1p0_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_1p5_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_5_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_10_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_20_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_30_20x20.dat'), 'rb')}
    r = requests.post(up_url, files=files)

    filename = 'dose_20x20'
    print '--------------------------------------'
    print 'This is test', filename
    print '--------------------------------------'
    #print options
    params = {}
    params['filename']     = filename
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['range_cut'] = 10
    params['target']       = options['target']['LowEnergy']
    params['flattening_filter'] = options['flattening_filter']['open port']
    params['energy']       = 5.90
    params['energy_sigma'] = 0.051
    params['spot_size_x']  = 0.6645
    params['spot_size_y']  = 0.7274
    params['beam_divergence_x'] = 0.0573
    params['beam_divergence_y'] = 0.0573
    params['jaw_position_y1'] = -10
    params['jaw_position_y2'] = 10
    params['jaw_position_x1'] = -10
    params['jaw_position_x2'] = 10
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 400
    params['phantom_size_y'] = 400
    params['phantom_size_z'] = 500
    params['phantom_voxels_x'] = 200
    params['phantom_voxels_y'] = 200
    params['phantom_voxels_z'] = 200
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -25
    if precision == 0:
        params['incident_particles'] = 100000
    else:
        params['incident_particles'] = 2000000
    params['brem_splitting'] = 200
    params['splitting_factor'] = 20

    params['phsp_record']  = None

    helper.print_params(params)

    print 'Submitting job:'
    submit = requests.post(url, data=params)
    helper._wait_for_jobs(url)
    print 'Done!'

    # plot dose distributions
    plt_url = url + '/fig/' + filename + '.dose'
    plt_params = {
        'direction' : 'z',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : -1,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Z_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.z.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_1p5_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.x_1p5.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_5_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.x_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_10_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.x_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_20_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.x_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_30_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.x_30.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 4,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_1p0_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.y_1p0.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_1p5_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.y_1p5.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_5_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.y_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_10_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.y_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_20_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.y_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_30_20x20.dat'
    }
    helper.get_file(plt_url, filename + '.y_30.png', params=plt_params,
            directory=directory)


def dose_10x10(url, 
               options,
               precision=0,
               code_version='10.2.p1',
               directory='smallfield'):
    """ Calculate dose to water for 10x10 6FFF beam; compare to measurement. """

    datadir = os.path.join(os.getenv('VL_TEST_DATADIR'), '6FFF')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    #upload measurement files
    up_url = url + '/upload'
    files = {'file': open(os.path.join(datadir, '6FFF_Z_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_1p5_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_5_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_10_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_20_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_30_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_1p0_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_1p5_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_5_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_10_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_20_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_30_10x10.dat'), 'rb')}
    r = requests.post(up_url, files=files)

    filename = 'dose_10x10'
    print '--------------------------------------'
    print 'This is test', filename
    print '--------------------------------------'
    #print options
    params = {}
    params['filename']     = filename
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['range_cut'] = 10
    params['target']       = options['target']['LowEnergy']
    params['flattening_filter'] = options['flattening_filter']['open port']
    params['energy']       = 5.90
    params['energy_sigma'] = 0.051
    params['spot_size_x']  = 0.6645
    params['spot_size_y']  = 0.7274
    params['beam_divergence_x'] = 0.0573
    params['beam_divergence_y'] = 0.0573
    params['jaw_position_y1'] = -5
    params['jaw_position_y2'] = 5
    params['jaw_position_x1'] = -5
    params['jaw_position_x2'] = 5
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 200
    params['phantom_size_y'] = 200
    params['phantom_size_z'] = 500
    params['phantom_voxels_x'] = 100
    params['phantom_voxels_y'] = 100
    params['phantom_voxels_z'] = 200
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -25
    if precision == 0:
        params['incident_particles'] = 100000
    else:
        params['incident_particles'] = 2000000
    params['brem_splitting'] = 200
    params['splitting_factor'] = 20

    params['phsp_record']  = None

    helper.print_params(params)

    print 'Submitting job:'
    submit = requests.post(url, data=params)
    helper._wait_for_jobs(url)
    print 'Done!'

    # plot dose distributions
    plt_url = url + '/fig/' + filename + '.dose'
    
    plt_params = {
        'direction' : 'z',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : -1,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Z_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.z.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_1p5_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.x_1p5.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_5_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.x_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_10_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.x_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_20_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.x_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_30_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.x_30.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 4,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_1p0_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.y_1p0.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_1p5_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.y_1p5.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_5_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.y_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_10_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.y_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_20_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.y_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_30_10x10.dat'
    }
    helper.get_file(plt_url, filename + '.y_30.png', params=plt_params,
            directory=directory)


def dose_6x6(url,
             options,
             precision=0,
             code_version='10.2.p1',
             directory='smallfield'):
    """ Calculate dose to water for 6x6 6FFF beam; compare to measurement. """

    datadir = os.path.join(os.getenv('VL_TEST_DATADIR'), '6FFF')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    #upload measurement files
    up_url = url + '/upload'
    files = {'file': open(os.path.join(datadir, '6FFF_Z_6x6.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_1p5_6x6.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_5_6x6.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_10_6x6.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_20_6x6.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_30_6x6.dat'), 'rb')}
    r = requests.post(up_url, files=files)

    filename = 'dose_6x6'
    print '--------------------------------------'
    print 'This is test', filename
    print '--------------------------------------'
    #print options
    params = {}
    params['filename']     = filename
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['range_cut'] = 10
    params['target']       = options['target']['LowEnergy']
    params['flattening_filter'] = options['flattening_filter']['open port']
    params['energy']       = 5.90
    params['energy_sigma'] = 0.051
    params['spot_size_x']  = 0.6645
    params['spot_size_y']  = 0.7274
    params['beam_divergence_x'] = 0.0573
    params['beam_divergence_y'] = 0.0573
    params['jaw_position_y1'] = -3.0
    params['jaw_position_y2'] = 3.0
    params['jaw_position_x1'] = -3.0
    params['jaw_position_x2'] = 3.0
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 150
    params['phantom_size_y'] = 150
    params['phantom_size_z'] = 500
    params['phantom_voxels_x'] = 75
    params['phantom_voxels_y'] = 75
    params['phantom_voxels_z'] = 200
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -25
    if precision == 0:
        params['incident_particles'] = 100000
    else:
        params['incident_particles'] = 2000000
    params['brem_splitting'] = 200
    params['splitting_factor'] = 20

    params['phsp_record']  = None

    helper.print_params(params)

    print 'Submitting job:'
    submit = requests.post(url, data=params)
    helper._wait_for_jobs(url)
    print 'Done!'

    # plot dose distributions
    plt_url = url + '/fig/' + filename + '.dose'

    plt_params = {
        'direction' : 'z',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : -1,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Z_6x6.dat'
    }
    helper.get_file(plt_url, filename + '.z.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_1p5_6x6.dat'
    }
    helper.get_file(plt_url, filename + '.x_1p5.png', params=plt_params,
            directory=directory)

    pltstr = ('?direction=x&voxel_x=-1&voxel_y=-1&voxel_z=20&'
              'average=3&compare=True&colormap=False&msmt_file=6FFF_X_5_6x6.dat')
    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_5_6x6.dat'
    }
    helper.get_file(plt_url, filename + '.x_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_10_6x6.dat'
    }
    helper.get_file(plt_url, filename + '.x_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_20_6x6.dat'
    }
    helper.get_file(plt_url, filename + '.x_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_30_6x6.dat'
    }
    helper.get_file(plt_url, filename + '.x_30.png', params=plt_params,
            directory=directory)


def dose_3x3(url,
             options,
             precision=0,
             code_version='10.2.p1',
             directory='smallfield'):
    """ Calculate dose to water for 3x3 6FFF beam; compare to measurement. """

    datadir = os.path.join(os.getenv('VL_TEST_DATADIR'), '6FFF')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    #upload measurement files
    up_url = url + '/upload'
    files = {'file': open(os.path.join(datadir, '6FFF_Z_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_1p5_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_5_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_10_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_20_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_X_30_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_1p0_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_1p5_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_5_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_10_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_20_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)
    files = {'file': open(os.path.join(datadir, '6FFF_Y_30_3x3.dat'), 'rb')}
    r = requests.post(up_url, files=files)

    filename = 'dose_3x3'
    print '--------------------------------------'
    print 'This is test', filename
    print '--------------------------------------'
    #print options
    params = {}
    params['filename']     = filename
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['range_cut'] = 10
    params['target']       = options['target']['LowEnergy']
    params['flattening_filter'] = options['flattening_filter']['open port']
    params['energy']       = 5.90
    params['energy_sigma'] = 0.051
    params['spot_size_x']  = 0.6645
    params['spot_size_y']  = 0.7274
    params['beam_divergence_x'] = 0.0573
    params['beam_divergence_y'] = 0.0573
    params['jaw_position_y1'] = -1.5
    params['jaw_position_y2'] = 1.5
    params['jaw_position_x1'] = -1.5
    params['jaw_position_x2'] = 1.5
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 100
    params['phantom_size_y'] = 100
    params['phantom_size_z'] = 500
    params['phantom_voxels_x'] = 50
    params['phantom_voxels_y'] = 50
    params['phantom_voxels_z'] = 200
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -25
    if precision == 0:
        params['incident_particles'] = 100000
    else:
        params['incident_particles'] = 2000000
    params['brem_splitting'] = 200
    params['splitting_factor'] = 20

    params['phsp_record']  = None

    helper.print_params(params)

    print 'Submitting job:'
    submit = requests.post(url, data=params)
    helper._wait_for_jobs(url)
    print 'Done!'

    # plot dose distributions
    plt_url = url + '/fig/' + filename + '.dose'

    plt_params = {
        'direction' : 'z',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : -1,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Z_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.z.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_1p5_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.x_1p5.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_5_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.x_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_10_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.x_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_20_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.x_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'x',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_30_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.x_30.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 4,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_1p0_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.y_1p0.png', params=plt_params,
            directory=directory)

    pltstr = ('?direction=y&voxel_x=-1&voxel_y=-1&voxel_z=6&'
              'average=3&compare=True&colormap=False&msmt_file=6FFF_Y_1p0_3x3.dat')
    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 6,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_1p5_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.y_1p5.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 20,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_5_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.y_5.png', params=plt_params,
            directory=directory)
    
    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 40,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_10_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.y_10.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 80,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_Y_20_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.y_20.png', params=plt_params,
            directory=directory)

    plt_params = {
        'direction' : 'y',
        'voxel_x'   : -1,
        'voxel_y'   : -1,
        'voxel_z'   : 120,
        'average'   : 3,
        'compare'   : 'True',
        'msmt_file' : '6FFF_X_30_3x3.dat'
    }
    helper.get_file(plt_url, filename + '.y_30.png', params=plt_params,
            directory=directory)


def fieldsize(url,
              precision=0,
              code_version='10.2.p1',
              directory='smallfield'):
    """ run all the field size tests """

    options = helper.get_home(url)
    if not os.path.isdir(directory):
        os.mkdir(directory)

    dose_3x3(url,
             options,
             precision=0,
             code_version=code_version,
             directory=directory)
    dose_6x6(url,
             options,
             precision=0,
             code_version=code_version,
             directory=directory)
    dose_10x10(url,
               options,
               precision=0,
               code_version=code_version,
               directory=directory)
    dose_20x20(url,
               options,
               precision=0,
               code_version=code_version,
               directory=directory)

