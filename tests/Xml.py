"""
   VirtuaLinac.Xml
   ~~~~~~~~~~~~~~~~~~~~~~~

   Configuration data for myvarian_photons tests

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# Xml.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os
import requests
import vl_api_helper as helper

class Xml(object):

    def __init__(self,
                 url,
                 code_version='10.2.p1',
                 precision=0,
                 directory='testing'):
        self.url = url
        self.up_url = url + '/upload'
        self.code_version = code_version
        self.options = helper.get_home(self.url)
        self.precision = precision
        self.directory = directory
        ## FIXME use envvar
        self.inputdir = os.path.join('..', 'api', 'tests', 'inputs')

        if os.getenv('VL_TEST_PHSPDIR'):
            self.phspdir = os.getenv('VL_TEST_PHSPDIR')
        else:
            print 'Set environment variable VL_TEST_PHSPDIR to the directory '
            'containing the MyVarian phase space files.'
            exit()


    def couch_rotation(self):
        """ couch rotation """

        filename = 'couchrotation'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        xmlfile = 'couchrot.xml'
        #upload phantom and xml files
        print(os.getcwd())
        print(os.path.join(self.inputdir, xmlfile))
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 100
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 6.5
        params['phantom_bool'] = 'True'
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -20
        params['phantom_voxels_x']   = 125
        params['phantom_voxels_y']   = 125
        params['phantom_voxels_z']   = 40
        params['phantom_size_x']     = 500
        params['phantom_size_y']     = 500
        params['phantom_size_z']     = 400
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 1
        params['trajectory_particles_mu'] = 100000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 10000
        params['brem_splitting'] = 10
        params['splitting_factor'] = 10
        params['filename'] = filename

        helper.print_params(params)
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'voxel_x'  : -1,
            'voxel_y'  : -1,
            'voxel_z'  : 3,
            'average'  : 5,
            'colormap' : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                        params=plt_params, directory=self.directory)


    def ct(self):
        """ xml trajectory for ct file and motion trajectory"""
        ## read an xml trajectory
        ## rotate the gantry and move the couch
        ## read a phantom file: cylinder with two materials
        ## 6e beam
        ## 2-d dose distributions for 3 axes
        filename = 'ct_motion'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        
        phantomfile = 'lung_index.phantom'
        xmlfile     = 'gantry_motion_2.xml'
        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir,phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 1000.0
        params['beam_type']    = 0
        params['energy']       = 6.2
        params['energy_sigma'] = 0.7
        params['flattening_filter'] = 7 ## open port
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 2
        params['trajectory_particles_mu'] = 10000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 100
        params['brem_splitting'] = 500
        params['splitting_factor'] = 100
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        plt_params['voxel_z']   = 140
        helper.get_file(plt_url, filename + '.2d.z.png', 
                    params=plt_params, directory=self.directory)


    def ct_fast(self):
        """ xml trajectory for ct file and motion trajectory"""
        ## read an xml trajectory
        ## rotate the gantry and move the couch
        ## read a phantom file: cylinder with two materials
        ## 6e beam
        ## 2-d dose distributions for 3 axes
        filename    = 'fast_ct'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        phantomfile = 'lung_small.phantom'
        xmlfile     = 'plan_8990.xml'
        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir,phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 1000.0
        params['beam_type']    = 0
        params['energy']       = 6.2
        params['energy_sigma'] = 0.7
        params['flattening_filter'] = 7 ## open port
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 0.1
        params['trajectory_particles_mu'] = 20
        if self.precision == 0:
            params['trajectory_particles_mu'] = 10
        params['brem_splitting'] = 100
        params['splitting_factor'] = 100
        #params['fast_simulation'] = 'True'
        params['phantom_dose_average'] = 5
        params['phantom_dose_average_type'] = 'triangle'
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        plt_params['voxel_z']   = 70
        helper.get_file(plt_url, filename + '.2d.z.png', 
                    params=plt_params, directory=self.directory)


    def electron_arc(self):
        """ xml trajectory for electron arc (basic), with reduced ssd """
        ## read an xml trajectory
        ## rotate the gantry and move the couch
        ## read a phantom file: cylinder with two materials
        ## 6e beam
        ## 2-d dose distributions for 3 axes
        filename    = 'electron_arc'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        
        xmlfile     = 'electron_arc2.xml'
        phantomfile = 'cylinder4.phantom'

        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 1
        params['foil1']       = self.options['foil1']['6E']
        params['foil2']       = self.options['foil2']['6E']
        params['energy']       = 6.84
        params['energy_sigma'] = 0.6
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -20
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 45
        params['trajectory_particles_mu'] = 2000000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 100000
        params['brem_splitting'] = None
        params['splitting_factor'] = None
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True',
            'average'   :  5
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        plt_params['voxel_z']   = 10
        helper.get_file(plt_url, filename + '.2d.z.png', 
                    params=plt_params, directory=self.directory)


    def imager_10x10(self):
        """ Simulate the imager by recording a phase space at -50 cm with
            a phantom in the beam """

        filename = 'imager_10x10'

        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 10
        params['target']       = self.options['target']['Imaging']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 2.5
        params['energy_sigma'] = 0.025
        params['spot_size_x']  = 0.0
        params['spot_size_y']  = 0.0
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['jaw_position_y1'] = -5
        params['jaw_position_y2'] =  5
        params['jaw_position_x1'] = -5
        params['jaw_position_x2'] =  5
        params['phantom_bool'] = None
        if self.precision == 0:
            params['incident_particles'] = 100000
        else:
            params['incident_particles'] = 1000000
        params['brem_splitting'] = 100

        params['phsp_record']  = 'True'
        params['phsp_position'] = '0'

        helper.print_params(params)

        ## delete existing files
        resp = requests.get(self.url + '/delete/' + filename + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + filename + '_field.header')

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap'  : 'True',
            'particle'  : 1,
            'yaxis'     : 'y',
            'xaxis'     : 'x',
            'factor'    : 'en',
            'xmin'      : '-10.0',
            'xmax'      : 10.0,
            'ymin'      : '-10.0',
            'ymax'      : 10.0,
            'ynumber_bins' : 200,
            'xnumber_bins' : 200
        }
        helper.get_file(plt_url, filename + '.phsp.2d.png', 
                    params=plt_params, directory=self.directory)

        plt_params['colormap'] = 'False'
        plt_params['condition'] = 'abs(y)<3'
        helper.get_file(plt_url, filename + '.phsp.xprofile.png', 
                    params=plt_params, directory=self.directory)


        ## download results
        helper.get_file(self.url+'/vl_files/'+filename+'.mac', filename+'.mac', 
                        directory=self.directory)
        helper.get_file(self.url+'/vl_files/'+filename+'_field.header',
                        filename+'_field.header', directory=self.directory)
        helper.get_file(self.url+'/vl_files/'+filename+'_field.phsp',
                        filename+'_field.phsp', directory=self.directory)


    def imager(self):
        """ Simulate the imager by recording a phase space at -50 cm with
            a phantom in the beam """

        filename = 'imager'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 10
        params['target']       = self.options['target']['Imaging']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 2.5
        params['energy_sigma'] = 0.025
        params['spot_size_x']  = 0.7
        params['spot_size_y']  = 0.7
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['jaw_position_y1'] = -20
        params['jaw_position_y2'] =  20
        params['jaw_position_x1'] = -20
        params['jaw_position_x2'] =  20
        params['phantom_bool'] = 'True'
        params['phantom_size_x'] = 100
        params['phantom_size_y'] = 100
        params['phantom_size_z'] = 200
        params['phantom_voxels_x'] = 1
        params['phantom_voxels_y'] = 1
        params['phantom_voxels_z'] = 1
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -10
        if self.precision == 0:
            params['incident_particles'] = 100000
        else:
            params['incident_particles'] = 1000000
        params['brem_splitting'] = 100
        params['splitting_factor'] = 10

        params['phsp_record']  = 'True'
        params['phsp_position'] = '-500'

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ynumber_bins' : 100,
            'xnumber_bins' : 100,
            'ymin'     : -40.0,
            'ymax'     : 40.0,
            'xmin'     : -40.0,
            'xmax'     : 40.0
        }

        helper.get_file(plt_url, filename + '.phsp.2d.png',
                params=plt_params, directory=self.directory)

    def imager_ct(self):
        """ Simulate the imager by recording a phase space at -50 cm with
            a CT phantom in the beam """

        filename = 'imager_ct'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        phantomfile = 'lung_index.phantom'

        print('Uploading phantom file.')
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        print('Done.')
        
        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 10
        params['target']       = self.options['target']['Imaging']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 2.5
        params['energy_sigma'] = 0.025
        params['spot_size_x']  = 0.7
        params['spot_size_y']  = 0.7
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['jaw_position_y1'] = -20
        params['jaw_position_y2'] =  20
        params['jaw_position_x1'] = -20
        params['jaw_position_x2'] =  20
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = 0
        if self.precision == 0:
            params['incident_particles'] = 10000
        else:
            params['incident_particles'] = 1000000
        params['brem_splitting'] = 200
        params['splitting_factor'] = 20
        #params['fast_simulation'] = 'True'

        params['phsp_record']  = 'True'
        params['phsp_position'] = '-500'

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap' : 'True',
            'cmap'     : 'Greys',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ynumber_bins' : 100,
            'xnumber_bins' : 100,
            'ymin'     : -40.0,
            'ymax'     : 40.0,
            'xmin'     : -40.0,
            'xmax'     : 40.0,
            'factor'   : 'en',
            'condition' : 'en<0.5'
        }

        helper.get_file(plt_url, filename + '.phsp.2d.png',
                params=plt_params, directory=self.directory)



    def inputphsp(self):
        """ xml trajectory with input phase space file (source) """

        filename    = 'inputphsp'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        beam        = '6X'
        phspfile    = 'TrueBeam_6X_v2_00'
        xmlfile     = 'gantry.xml'
        phantomfile = 'insert_double.phantom'

        #upload phantom, xml, phsp files
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        print 'Uploading phase space file.'
        files = {'file': open(os.path.join(self.phspdir, beam,
                              phspfile + '.IAEAphsp'), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.phspdir, beam,
                              phspfile + '.IAEAheader'), 'rb')}
        r = requests.post(self.up_url, files=files)
        print 'Done.'

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        #params['energy']       = 6.5
        params['phsp_input_bool'] = 'True'
        params['phsp_input_file'] = phspfile + '.IAEAphsp'
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -25
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 10
        params['trajectory_particles_mu'] = 100000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        #params['brem_splitting'] = 10
        params['splitting_factor'] = 10
        params['filename'] = filename

        helper.print_params(params)
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'average'   : 5,
            'colormap'  :'True'
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        helper.get_file(plt_url, filename + '.2d.z.png', 
                params=plt_params, directory=self.directory)


    def jaw_orientation(self):
        """ Set jaw positions asymmetrically and compare phase space, phantom"""
        ### then repeat with MLC, should get same answer

        filename = 'jaw_orientation'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 10
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 5.90
        params['energy_sigma'] = 0.051
        params['spot_size_x']  = 0.6645
        params['spot_size_y']  = 0.7274
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['jaw_position_y1'] = -10
        params['jaw_position_y2'] =  20
        params['jaw_position_x1'] =   0
        params['jaw_position_x2'] =  20
        params['phantom_bool'] = 'True'
        params['phantom_size_x'] = 500
        params['phantom_size_y'] = 500
        params['phantom_size_z'] = 200
        params['phantom_voxels_x'] = 50
        params['phantom_voxels_y'] = 50
        params['phantom_voxels_z'] = 20
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -10
        if self.precision == 0:
            params['incident_particles'] = 10000
        else:
            params['incident_particles'] = 100000
        params['brem_splitting'] = 100
        params['splitting_factor'] = 10

        params['phsp_record']  = 'True'
        params['phsp_position'] = '-500'

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot dose distributions
        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'zplot'     : '3',
            'average'   : '5',
            'colormap'  : '1'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                params = plt_params, directory=self.directory)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ynumber_bins' : 100,
            'xnumber_bins' : 100,
            'ymin'     : -40.0,
            'ymax'     : 40.0,
            'xmin'     : -40.0,
            'xmax'     : 40.0
        }
        helper.get_file(plt_url, filename + '.phsp.2d.png', 
                params=plt_params, directory=self.directory)

        ############## mlc part ################

        xmlfile  = 'asymm_mlc.xml'
        #upload xml file specifying mlc positions
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filename = 'jaw_orientation_mlc'

        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 10
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 5.90
        params['energy_sigma'] = 0.051
        params['spot_size_x']  = 0.6645
        params['spot_size_y']  = 0.7274
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['jaw_position_y1'] = -20
        params['jaw_position_y2'] =  20
        params['jaw_position_x1'] = -20
        params['jaw_position_x2'] =  20
        params['trajectory_bool'] = True
        params['trajectory_beamlets_mu'] = 1
        if self.precision == 0:
            params['trajectory_particles_mu'] = 10000
        else:
            params['trajectory_particles_mu'] = 100000
        ## recall leaf bank A is under jaw X2
        params['trajectory_file'] = xmlfile
        
        params['phantom_bool'] = 'True'
        params['phantom_size_x'] = 500
        params['phantom_size_y'] = 500
        params['phantom_size_z'] = 200
        params['phantom_voxels_x'] = 50
        params['phantom_voxels_y'] = 50
        params['phantom_voxels_z'] = 20
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -10
        params['brem_splitting'] = 100
        params['splitting_factor'] = 10

        params['phsp_record']  = 'True'
        params['phsp_position'] = '-500'

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot dose distributions
        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'zplot'     : '3',
            'average'   : '5',
            'colormap'  : '1'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                params = plt_params, directory=self.directory)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ynumber_bins' : 100,
            'xnumber_bins' : 100,
            'ymin'     : -40.0,
            'ymax'     : 40.0,
            'xmin'     : -40.0,
            'xmax'     : 40.0
        }
        helper.get_file(plt_url, filename + '.phsp.2d.png', 
                params=plt_params, directory=self.directory)



    def mlc(self):
        """ mlc trajectory """

        filename = 'mlc'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        xmlfile  = 'mlc.xml'
        #upload xml file specifying mlc positions
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['energy']       = 6.5
        params['jaw_position_y1'] = -20
        params['jaw_position_y2'] =  20
        params['jaw_position_x1'] = -20
        params['jaw_position_x2'] =  20

        params['phantom_bool'] = None#'True'
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        #print('Using trajectory file: ', ind1)
        params['trajectory_beamlets_mu'] = 1
        if self.precision == 0:
            params['trajectory_particles_mu'] = 10000
        else:
            params['trajectory_particles_mu'] = 500000
        params['brem_splitting'] = 100
        params['splitting_factor'] = 1#0
        params['filename'] = filename
        params['phsp_record']  = 'True'
        params['phsp_position'] = '0'

        helper.print_params(params)
        
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ymin'     : -40,
            'ymax'     : 40,
            'xmin'     : -40,
            'xmax'     : 40,
            'xnumber_bins' : 100,
            'ynumber_bins' : 100
        }
        helper.get_file(plt_url, filename + '.2d.phsp.png',
                params=plt_params, directory=self.directory)


    def randomphantom(self):
        """ test of phantom materials and densities """
        ## read an xml trajectory
        ## rotate the gantry and move the couch
        ## read a phantom file: cylinder with two materials
        ## 6e beam
        ## 2-d dose distributions for 3 axes
        filename    = 'randomphantom'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        phantomfile = 'random3.phantom'
        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['energy']       = 6.18
        params['energy_sigma'] = 0.053
        params['spot_size_x']  = 0.6866
        params['spot_size_y']  = 0.7615
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -25
        params['brem_splitting'] = 100
        params['splitting_factor'] = 20
        if self.precision == 0:
            params['incident_particles'] = 10000
        else:
            params['incident_particles'] = 1000000

        params['random_number_seed'] = 9223372036854775807
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'y',
            'colormap'  : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.y.png', 
                        params=plt_params, directory=self.directory)

        plt_params['direction'] = 'x'
        helper.get_file(plt_url, filename + '.2d.x.png', 
                        params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        helper.get_file(plt_url, filename + '.2d.z.png', 
                        params=plt_params, directory=self.directory)


    def sliding_jaw(self):
        """ sliding jaw trajectory """

        filename = 'slidingjaw'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        xmlfile  = 'slidingjaw.xml'
        #upload xml file specifying sliding jaw positions
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 100
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['energy']       = 6.5
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = None
        params['phantom_size_x'] = 500
        params['phantom_size_y'] = 500
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 100
        params['phantom_voxels_y'] = 100
        params['phantom_voxels_z'] = 40
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -20
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 1
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        else:
            params['trajectory_particles_mu'] = 20000
        params['brem_splitting'] = 100
        params['splitting_factor'] = 10
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)
        
        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'voxel_z'   : 2,
            'colormap'  : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                params=plt_params, directory=self.directory)
 
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : 2,
            'colormap'  : 'False',
            'average'   : 5
        }
       
        helper.get_file(plt_url, filename + '.x.png', 
                params=plt_params, directory=self.directory)


    def smallphsp(self):
        """ xml trajectory with small input phase space file (source) """
        ## testing reuse of the same input many times. 
        ## output should be streaky
        ## first create a small phase space file. then use it as input.
        filename = 'smallphsp'
        source_phsp = 'test_createphsp'
        
        ## delete existing files
        resp = requests.get(self.url + '/delete/' + source_phsp + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + source_phsp + '_field.header')

        ## first generate the small phshp
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        #upload phantom, xml, phsp files
        xmlfile      = 'gantry.xml'
        phantomfile = 'insert_double.phantom'
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 6.2
        params['incident_particles'] = 10000
        #params['brem_splitting'] = 10
        #params['splitting_factor'] = 10
        params['filename'] = source_phsp
        params['phsp_record'] = 'True'
        params['phsp_position'] = 733

        helper.print_params(params)
        print 'Submitting job to create source phsp:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['phsp_input_bool'] = 'True'
        params['phsp_input_file'] = source_phsp+'_field.phsp'
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 10
        params['trajectory_particles_mu'] = 100000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        #params['brem_splitting'] = 10
        params['splitting_factor'] = 10
        params['filename'] = filename

        helper.print_params(params)
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 1
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                params=plt_params, directory=self.directory)
       
        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        helper.get_file(plt_url, filename + '.2d.z.png', 
                params=plt_params, directory=self.directory)

    def starshot(self):
        """ starshot trajectory """

        filename = 'starshot'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        #upload xml file specifying starshot
        xmlfile = 'starshot.xml'
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 100
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['energy']       = 6.5
        params['phantom_bool'] = None#'True'
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 1
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        else:
            params['trajectory_particles_mu'] = 20000
        params['phsp_record']  = 'True'
        params['phsp_position'] = '-500'

        params['brem_splitting'] = 100
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'fluence'  : 'True',
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ymin'     : -30,
            'ymax'     : 30,
            'xmin'     : -30,
            'xmax'     : 30,
            'xnumber_bins' : 100,
            'ynumber_bins' : 100
        }
        helper.get_file(plt_url, filename + '.2d.phsp.png',
                params=plt_params, directory=self.directory)


    def xml_basic(self):
        """ xml trajectory """

        filename    = 'xml_simple'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        #upload phantom and xml files
        xmlfile     = 'gantry.xml'
        phantomfile = 'insert_double.phantom'
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 6.5
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -25
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 10
        params['trajectory_particles_mu'] = 100000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        params['brem_splitting'] = 10
        params['splitting_factor'] = 10
        params['filename'] = filename 

        helper.print_params(params)
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True',
            'average'   : 5
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                params=plt_params, directory=self.directory)
        
        plt_params['direction'] = 'z'
        helper.get_file(plt_url, filename + '.2d.z.png', 
                params=plt_params, directory=self.directory)

    def custom_target(self):
        """ target material from api """

        filename = 'custom_target'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        ## delete existing files
        resp = requests.get(self.url + '/delete/' + filename + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + filename + '_field.header')
        
        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['Custom']
        params['target_custom_material'] = 'G4_Au' #BAKELITE'
        params['target_custom_thickness'] = 3
        params['target_custom_radius'] = 5
        params['target_custom_material2'] = 'G4_Pb'
        params['target_custom_layer2_vertices'] = '[0,0],[5,0],[5,-1],[8,-1],[9,5],[0,5]'

        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 15.5
        params['jaw_position_y1'] = -20
        params['jaw_position_y2'] =  20
        params['jaw_position_x1'] = -20
        params['jaw_position_x2'] =  20

        params['phantom_bool'] = None#'True'
        if self.precision == 0:
            params['incident_particles'] = 10000
        else:
            params['incident_particles'] = 100000
        params['brem_splitting'] = 100
        params['splitting_factor'] = 1#0
        params['filename'] = filename
        params['phsp_record']  = 'True'
        params['phsp_position'] = '0'

        helper.print_params(params)
        
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ymin'     : -40,
            'ymax'     : 40,
            'xmin'     : -40,
            'xmax'     : 40,
            'xnumber_bins' : 100,
            'ynumber_bins' : 100
        }
        helper.get_file(plt_url, filename + '.2d.phsp.png',
                params=plt_params, directory=self.directory)

    def electron_cutout(self):
        """ Simulate the an electron applicator and cutout """
        ## use an Geant4 material for foil1

        filename = 'electron_cutout'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut'] = 100
        params['beam_type'] = 1
        params['foil1']       = 8
        params['foil1_custom_material'] = 'G4_Pt'
        params['foil1_custom_thickness'] = 0.1
        params['foil2']       = 0
        params['energy']       = 6
        params['energy_sigma'] = 0.025
        params['spot_size_x']  = 0.7
        params['spot_size_y']  = 0.7
        params['beam_divergence_x'] = 0
        params['beam_divergence_y'] = 0
        params['jaw_position_y1'] = -10
        params['jaw_position_y2'] =  10
        params['jaw_position_x1'] = -10
        params['jaw_position_x2'] =  10
        params['phantom_bool'] = 'True'
        params['phantom_size_x'] = 200
        params['phantom_size_y'] = 200
        params['phantom_size_z'] = 100
        params['phantom_voxels_x'] = 100
        params['phantom_voxels_y'] = 100
        params['phantom_voxels_z'] = 20
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -5
        params['cutout'] = 'True'
        params['applicator'] = 4
        params['cutout_vertices'] = '[1,-1.5],[3,3],[5,3],[2,-4],[-2,-4],[-5,3],[-3,3],[-1,-1.5]'


        if self.precision == 0:
            params['incident_particles'] = 100000
        else:
            params['incident_particles'] = 1000000


        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'voxel_x'  : -1,
            'voxel_y'  : -1,
            'voxel_z'  : 2,
            'average'  : 5,
            'colormap' : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                        params=plt_params, directory=self.directory)

    def electron_cutout_varianI(self):
        """ Simulate the an electron applicator and cutout """

        filename = 'electron_cutout_varianI'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'

        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 100
        params['beam_type'] = 1
        params['foil1']       = 0
        params['foil2']       = 0
        params['energy']       = 6
        params['energy_sigma'] = 0.025
        params['spot_size_x']  = 0.7
        params['spot_size_y']  = 0.7
        params['beam_divergence_x'] = 0
        params['beam_divergence_y'] = 0
        params['collimator_rotation'] = 180
        params['jaw_position_y1'] = -10
        params['jaw_position_y2'] =  10
        params['jaw_position_x1'] = -10
        params['jaw_position_x2'] =  10
        params['phantom_bool'] = 'True'
        params['phantom_size_x'] = 200
        params['phantom_size_y'] = 200
        params['phantom_size_z'] = 100
        params['phantom_voxels_x'] = 100
        params['phantom_voxels_y'] = 100
        params['phantom_voxels_z'] = 20
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -5
        params['cutout'] = 'True'
        params['applicator'] = 4
        #params['cutout_vertices'] = '[-2.4,-2],[-1.25,0],[1.55,8.35],[1.8,8.45],[1.95,8.3],[1.3,0.2],[1.35,-.05],[2.7,-.45],[2.85,-.5],[2.75,-.65],[-0.1,-1.35],[0,-2.2],[.65,-2.55],[.95,-3.25],[.6,-3.95],[-0.05,-4.2],[-.65,-3.95],[-1.0,-3.25],[-.75,-2.55],[-.05,-2.2],[-.15,-1.35]'
        params['cutout_vertices'] = '[-2.4,-4],[-1.25,-2],[1.55,6.35],[1.8,6.45],[1.95,6.3],[1.3,-1.8],[1.35,-2.05],[2.7,-2.45],[2.85,-2.5],[2.75,-2.65],[-0.1,-3.35],[0,-4.2],[.65,-4.55],[.95,-5.25],[.6,-5.95],[-0.05,-6.2],[-.65,-5.95],[-1.0,-5.25],[-.75,-4.55],[-.05,-4.2],[-.15,-3.35]'
        params['cutout_thickness'] = 15
        params['cutout_bevel_factor'] = 0.99
        #params['cutout_material'] = 'G4_Pb'

        if self.precision == 0:
            params['incident_particles'] = 100000
        else:
            params['incident_particles'] = 10000000

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'voxel_x'  : -1,
            'voxel_y'  : -1,
            'voxel_z'  : 2,
            'average'  : 5,
            'colormap' : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                        params=plt_params, directory=self.directory)
        # plot phase space
        #plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        #plt_params = {
        #    'colormap'  : 'True',
        #    'particle'  : 1,
        #    'yaxis'     : 2,
        #    'xaxis'     : 1,
        #    'factor'    : 'en',
        #    'xmin'      : '-10.0',
        #    'xmax'      : 10.0,
        #    'ymin'      : '-10.0',
        #    'ymax'      : 10.0,
        #    'ynumber_bins' : 200,
        #    'xnumber_bins' : 200
        #}
        #helper.get_file(plt_url, filename + '.phsp.2d.png', 

    def varian_script(self):
        """ couch motion to spell Varian """

        filename = 'varianscript'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        xmlfile = 'varianscript.xml'
        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        #params['fast_simulation'] = 'True'
        params['range_cut']  = 1000
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 6.5
        params['phantom_bool'] = 'True'
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -10
        params['phantom_voxels_x']   = 200
        params['phantom_voxels_y']   = 70
        params['phantom_voxels_z']   = 10
        params['phantom_size_x']     = 600
        params['phantom_size_y']     = 200
        params['phantom_size_z']     = 200
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 10
        params['trajectory_particles_mu'] = 20000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        params['brem_splitting'] = 200
        params['splitting_factor'] = 20
        params['filename'] = filename

        helper.print_params(params)
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'voxel_x'  : -1,
            'voxel_y'  : -1,
            'voxel_z'  : 1,
            'average'  : 1,
            'colormap' : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.z.png', 
                        params=plt_params, directory=self.directory)


    def ct_small(self):
        """ xml trajectory for ct file and motion trajectory"""
        ## read an xml trajectory
        ## rotate the gantry and move the couch
        ## read a phantom file: cylinder with two materials
        ## 6e beam
        ## 2-d dose distributions for 3 axes
        filename    = 'small_ct'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        phantomfile = 'lung_small.phantom'
        xmlfile     = 'gantry_motion_2.xml'
        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir,phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 1000.0
        params['beam_type']    = 0
        params['energy']       = 6.2
        params['energy_sigma'] = 0.7
        params['flattening_filter'] = 7 ## open port
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 0.1
        params['trajectory_particles_mu'] = 200
        if self.precision == 0:
            params['trajectory_particles_mu'] = 10
        params['brem_splitting'] = 500
        params['splitting_factor'] = 100
        params['fast_simulation'] = 'True'
        params['kill_primColl'] = 100
        params['kill_shieldColl'] = 200
        params['kill_jaw'] = 200
        params['kill_mlc'] = 200
        params['kill_phantomAll'] = 100
        params['kill_phantomElec'] = 100
        params['phantom_dose_average_number'] = 5
        params['phantom_dose_average_type'] = 'triangle'
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True'
        }
        helper.get_file(plt_url, filename + '.2d.x.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png', 
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        plt_params['voxel_z']   = 70
        helper.get_file(plt_url, filename + '.2d.z.png', 
                    params=plt_params, directory=self.directory)



    def latch(self):
        """ test that latch information propagates on write/read phsp """
        ## 1. record phsp at isocenter; plot latches (log vol, process)
        ## 2. record phsp at 733; read in and record another at 0; compare plots 
        filename = 'latch_compare_1'

        ## delete existing files
        resp = requests.get(self.url + '/delete/' + filename + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + filename + '_field.header')

        ## first record phsp at 0
        print '--------------------------------------'
        print 'This is test', filename, '(part 1)'
        print '--------------------------------------'

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['shielding_simulate'] = True
        params['energy']       = 6.2
        if self.precision == 0:
            params['incident_particles'] = 10000
        else:
            params['incident_particles'] = 100000
        params['brem_splitting'] = 100
        params['filename'] = filename
        params['phsp_record'] = 'True'
        params['phsp_position'] = 0

        helper.print_params(params)
        print 'Submitting job.'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'particle' : 1,
            'xaxis'    : 'extra_longs[1]',
            'xmin'     : 0,
            'xmax'     : 110,
            'xnumber_bins' : 110,
            'condition': 'abs(x) < 30 and abs(y) < 30'
        }
        helper.get_file(plt_url, filename + '.latch.png',
                params=plt_params, directory=self.directory)

        ## then with intermediate phsp
        print '--------------------------------------'
        print 'This is test', filename, '(part 2)'
        print '--------------------------------------'

        filename = 'latch_compare_2'
        intermediate_phsp = filename + '_intermediate'

        ## delete existing files
        resp = requests.get(self.url + '/delete/' + filename + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + filename + '_field.header')
        resp = requests.get(self.url + '/delete/' + intermediate_phsp + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + intermediate_phsp + '_field.header')

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['shielding_simulate'] = True
        params['energy']       = 6.2
        if self.precision == 0:
            params['incident_particles'] = 10000
        else:
            params['incident_particles'] = 100000
        params['brem_splitting'] = 100
        params['filename'] = intermediate_phsp
        params['phsp_record'] = 'True'
        params['phsp_position'] = 733

        helper.print_params(params)
        print 'Submitting job.'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        ## then read phsp in

        print 'read phsp in: filename is', filename
        params = {}
        params['phsp_input_bool'] = 'True'
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['6X']
        params['shielding_simulate'] = True

        print 'read phsp in: file to read is', intermediate_phsp + '_field.phsp'
        params['phsp_input_file'] = intermediate_phsp + '_field.phsp'
        params['phsp_record'] = 'True'
        params['phsp_position'] = 0
        params['phsp_recycle'] = 1
        params['filename'] = filename

        helper.print_params(params)
        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'particle' : 1,
            'xaxis'    : 'extra_longs[1]',
            'xmin'     : 0,
            'xmax'     : 110,
            'xnumber_bins' : 110,
            #'ylog'     : 'True',
            #'ymin'     : '0.001',
            'condition': 'abs(x) < 30 and abs(y) < 30'
        }
        helper.get_file(plt_url, filename + '.latch.png',
                params=plt_params, directory=self.directory)

    def einstein(self):
        """ picture of einstein """

        filename = 'einstein'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        #upload xml file
        xmlfile = 'einstein1.xml'
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 1000
        params['beam_type']    = 0
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = 7
        params['energy']       = 6.5
        params['phantom_bool'] = None#'True'
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 1
        if self.precision == 0:
            params['trajectory_particles_mu'] = 100
        else:
            params['trajectory_particles_mu'] = 1000
        params['phsp_record']  = 'True'
        params['phsp_position'] = '0'

        params['brem_splitting'] = 200
        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        plt_params = {
            'fluence'  : 'True',
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'y',
            'xaxis'    : 'x',
            'ymin'     : 22,  # FIXME xml is backwards!
            'ymax'     : -22,
            'xmin'     : 22,
            'xmax'     : -22,
            'xnumber_bins' : 100,
            'ynumber_bins' : 100,
            'aspect'   : 'equal',
            'cmap'     : 'Greys'
        }
        helper.get_file(plt_url, filename + '.2d.phsp.png',
                params=plt_params, directory=self.directory)

    def phantom_scatter(self):
        """ Record scatter from a phantom, using spherical phsp centered at
            iso and highly asymmetric phantom """

        filename = 'phantom_scatter'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        phantomfile = 'halflead.phantom'

        print('Uploading phantom file.')
        files = {'file': open(os.path.join(self.inputdir, phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        print('Done.')
        
        params = {}
        params['filename']     = filename
        params['code_version'] = 0
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut'] = 10
        params['target']       = self.options['target']['LowEnergy']
        params['flattening_filter'] = self.options['flattening_filter']['open port']
        params['energy']       = 6.15
        params['energy_sigma'] = 0.025
        params['spot_size_x']  = 0.7
        params['spot_size_y']  = 0.7
        params['beam_divergence_x'] = 0.0573
        params['beam_divergence_y'] = 0.0573
        params['jaw_position_y1'] = -2
        params['jaw_position_y2'] =  2
        params['jaw_position_x1'] = -2
        params['jaw_position_x2'] =  2
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = 0
        if self.precision == 0:
            params['incident_particles'] = 100000
        else:
            params['incident_particles'] = 1000000
        params['brem_splitting'] = 200
        params['splitting_factor'] = 1
        #params['fast_simulation'] = 'True'

        params['phsp_leakage']  = 'True'

        params['phsp_spherical_position_x'] = 0
        params['phsp_spherical_position_y'] = 0
        params['phsp_spherical_position_z'] = 0
        params['phsp_spherical_radius'] = 400

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        # plot phase space
        plt_url = self.url + '/figphsp/' + filename + '_leakage.phsp'
        plt_params = {
            'colormap' : 'True',
            'particle' : 1,
            'yaxis'    : 'z',
            'xaxis'    : 'x',
            'ynumber_bins' : 100,
            'xnumber_bins' : 100,
            'ymin'     : -45.0,
            'ymax'     :  45.0,
            'xmin'     : -45.0,
            'xmax'     :  45.0,
            'condition' : 'abs(z)<39'
        }

        helper.get_file(plt_url, filename + '.phsp.2d.png',
                params=plt_params, directory=self.directory)


    def all_xml(self):
        #self.phantom_scatter()
        #self.jaw_orientation()
        #self.couch_rotation()
        #self.latch()
        self.electron_cutout_varianI()
        ##self.electron_cutout()
        self.imager_ct()
        self.custom_target()
        self.electron_arc()
        self.imager_10x10()
        self.imager()
        self.inputphsp()
        self.mlc()
        self.randomphantom()
        self.sliding_jaw()
        self.smallphsp()
        self.starshot()
        self.xml_basic()
        self.ct_small()
        self.ct_fast()
        self.varian_script()
        self.einstein()
        self.ct()


