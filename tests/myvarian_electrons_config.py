"""
   VirtuaLinac.myvarian_electrons_config
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Configuration for simulations starting from MyVarian electron phsp

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# myvarian_photons.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os

def config(phsp_index=0):
    if int(phsp_index) < 0 or int(phsp_index) > 55:
        print 'Phase space index', phsp_index, 'out of range!'
        exit()
        return 1
    phsp_index = '{0:02}'.format(int(phsp_index))

    phspdir = os.getenv('VL_TEST_PHSPDIR')
    if not phspdir:
        print 'Set the environment variable VL_TEST_PHSPDIR to the directory'
        print 'where the input phase space files are.'
        exit()
    
    datadir = os.getenv('VL_TEST_DATADIR')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    config = {
        'phsp_dir' : phspdir,
        'source_phsp' : {
            '6E'  : 'TrueBeam_06E_' + phsp_index + '.IAEAphsp',
            '9E'  : 'TrueBeam_09E_' + phsp_index + '.IAEAphsp',
            '12E' : 'TrueBeam_12E_' + phsp_index + '.IAEAphsp',
            '15E' : 'TrueBeam_15E_' + phsp_index + '.IAEAphsp',
            '16E' : 'TrueBeam_16E_' + phsp_index + '.IAEAphsp',
            '18E' : 'TrueBeam_18E_' + phsp_index + '.IAEAphsp',
            '20E' : 'TrueBeam_20E_' + phsp_index + '.IAEAphsp',
            '22E' : 'TrueBeam_22E_' + phsp_index + '.IAEAphsp'
        },
       
        'data_dir' : datadir,

        'pdd' : {
            '6E'  : '6E_Z.dat',
            '9E'  : '9E_Z.dat',
            '12E' : '12E_Z.dat',
            '15E' : '15E_Z.dat',
            '16E' : '16E_Z.dat',
            '18E' : '18E_Z.dat',
            '20E' : '20E_Z.dat',
            '22E' : '22E_Z.dat'
        },

        'prf' : {
            '6E'  : '6E_X_dmax.dat',
            '9E'  : '9E_X_dmax.dat',
            '12E' : '12E_X_dmax.dat',
            '15E' : '15E_X_dmax.dat',
            '16E' : '16E_X_dmax.dat',
            '18E' : '18E_X_dmax.dat',
            '20E' : '20E_X_dmax.dat',
            '22E' : '22E_X_dmax.dat'
        },

        'airprf' : {
            '6E'  : '6E_X_air.dat',
            '9E'  : '9E_X_air.dat',
            '12E' : '12E_X_air.dat',
            '15E' : '15E_X_air.dat',
            '16E' : '16E_X_air.dat',
            '18E' : '18E_X_air.dat',
            '20E' : '20E_X_air.dat',
            '22E' : '22E_X_air.dat'
        },

        'voxel_z' : {
            '6E'  : 6,
            '9E'  : 9,
            '12E' : 13,
            '15E' : 16,
            '16E' : 16,
            '18E' : 16,
            '20E' : 15,
            '22E' : 14
        },

        'phantom_z' : {
            '6E'  : 50,
            '9E'  : 50,
            '12E' : 50,
            '15E' : 60,
            '16E' : 60,
            '18E' : 70,
            '20E' : 70,
            '22E' : 75
        }
    }

    return config


def params():
    params = {}
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['beam_type']    = 1
    params['range_cut'] = 10
    params['phsp_input_bool'] = True
    params['phsp_recycle']  = 1
    params['jaw_position_y1'] = -20
    params['jaw_position_y2'] =  20
    params['jaw_position_x1'] = -20
    params['jaw_position_x2'] =  20
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 500
    params['phantom_size_y'] = 500
    params['phantom_voxels_x'] = 100
    params['phantom_voxels_y'] = 100
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['brem_splitting'] = None
    params['splitting_factor'] = None

    return params
