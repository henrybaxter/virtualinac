#!/usr/bin/env python
"""
   VirtuaLinac.create_ct
   ~~~~~~~~~~~~~~~~~~~~~~~

   Convert a set of DICOM CT files to a VirtuaLinac phantom file

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# create_ct.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import glob
import sys
import os
import dicom  # pydicom
import numpy as np


def read_in(directory):
    directory = os.path.abspath(directory)
    pattern = directory + '/CT.*'
    print('\n--------------------')
    print('Reading files from {}'.format(pattern))
    print('--------------------\n')

    # get the files
    paths = sorted(glob.glob(directory + '/CT.*'))
    if not paths:
        print("No CT.* files found in directory, aborting")
        sys.exit(1)
    print('Found {} files'.format(len(paths)))

    # want the superior slices to be most positive Y value
    paths = reversed(paths)

    ct = []
    ds = None
    for path in paths:
        ds = dicom.read_file(path)
        ct.append(ds.pixel_array)

    ny = len(ct)
    nx, nz = ds.pixel_array.shape

    sizex = float(ds.PixelSpacing[0])
    sizey = float(ds.SliceThickness)
    sizez = float(ds.PixelSpacing[1])

    ct = np.array(ct)

    # create a new array that can be reshaped
    ct_reshape = []
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                ct_reshape.append(ct[j, k, i])

    # reshape the array so that x,y,z are as VirtuaLinac expects
    ct = np.array(ct_reshape)
    ct = np.reshape(ct, (nx, ny, nz))

    # convert data type to 32 bit integer
    ct = ct.astype(np.dtype('int32'))

    # convert to HU from CT#
    # ct -= 1000

    print('Number of voxels    : {0} {1} {2}'.format(nx, ny, nz))
    print('Size of voxels [mm] : {0} {1} {2}'.format(sizex, sizey, sizez))

    return ct, sizex, sizey, sizez


def outside_to_air(ct1):
    """ convert points outside body (and couch etc.) to HU = -1000 """
    # for each pixel on boundary of ct: if pixul HU less than threshold,
    # set pixel HU value to -1000. Proceed inwards in CT, setting pixel values
    # to -1000 until a pixel with HU >= threshold is reached.

    print('\n--------------------------------------')
    print('Converting points outside body to air.')
    print('--------------------------------------\n')

    ct2 = ct1.copy()
    threshold = -400
    nx, ny, nz = ct2.shape

    # iterate over X slices
    for i in range(nx):
        if not i % 10:
            print('Now processing X value:', i)
        ctslice = ct2[i, :, :]
        # check lines along Z
        for j in range(ny):
            line = ctslice[j, :]
            for k in range(nz):
                if k == 0:
                    if line[k] < threshold:
                        ct2[i, j, k] = -1000
                elif k == nz - 1:
                    if line[k] < threshold:
                        ct2[i, j, k] = -1000
                else:
                    if line[k] < threshold and line[k-1] == -1000:
                        ct2[i, j, k] = -1000
                    else:
                        break
            for k in range(nz-2, 0, -1):
                if line[k] < threshold and line[k+1] == -1000:
                    ct2[i, j, k] = -1000
                else:
                    break
        # check lines along Y
        for k in range(nz):
            line = ctslice[:, k]
            for j in range(ny):
                if j == 0:
                    if line[j] < threshold:
                        ct2[i, j, k] = -1000
                elif j == ny - 1:
                    if line[j] < threshold:
                        ct2[i, j, k] = -1000
                else:
                    if line[j] < threshold and line[j-1] == -1000:
                        ct2[i, j, k] = -1000
                    else:
                        break
            for j in range(ny-2, 0, -1):
                if line[j] < threshold and line[j+1] == -1000:
                    ct2[i, j, k] = -1000
                else:
                    break

    # iterate over Z slices
    for k in range(nz):
        if not k % 10:
            print('Now processing Z value:', k)
        ctslice = ct2[:, :, k]
        # check lines along X
        for j in range(ny):
            line = ctslice[:, j]
            for i in range(nx):
                if i == 0:
                    if line[i] < threshold:
                        ct2[i, j, k] = -1000
                elif i == nx - 1:
                    if line[i] < threshold:
                        ct2[i, j, k] = -1000
                else:
                    if line[i] < threshold and line[i-1] == -1000:
                        ct2[i, j, k] = -1000
                    else:
                        break
            for i in range(nx-2, 0, -1):
                if line[i] < threshold and line[i+1] == -1000:
                    ct2[i, j, k] = -1000
                else:
                    break

    print('Done.')
    return ct2


def inside_to_lung(ct1):
    """for voxels inside the body that have HU = -1000, change to HU = -999

    # for every voxel in CT: if HU = -1000, check if we can move outwards in
    ## all 6 directions and hit voxel with HU > -1000. If so, the current
    ## voxel is inside the body. Convert HU to -999.
    ## If the edge of CT is hit (in any direction), keep as -1000
    """

    print('\n-----------------------------------------------')
    print('Converting voxels inside body from air to lung.')
    print('-----------------------------------------------\n')

    ct2 = ct1.copy()
    nx, ny, nz = ct2.shape
    for i in range(1, nx-1):
        if not i % 10:
            print('Now processing X = {0} / {1}'.format(i, nx))
        for j in range(1, ny-1):
            for k in range(1, nz-1):
                outside = False
                if ct2[i, j, k] == -1000:
                    for ii in range(i):
                        if ct2[ii, j, k] > -1000:
                            break
                    else:
                        outside = True
                    if not outside:
                        for ii in range(i + 1, nx):
                            if ct2[ii, j, k] > -1000:
                                break
                        else:
                            outside = True
                    if not outside:
                        for jj in range(j):
                            if ct2[i, jj, k] > -1000:
                                break
                        else:
                            outside = True
                    if not outside:
                        for jj in range(j + 1, ny):
                            if ct2[i, jj, k] > -1000:
                                break
                        else:
                            outside = True
                    if not outside:
                        for kk in range(k):
                            if ct2[i, j, kk] > -1000:
                                break
                        else:
                            outside = True
                    if not outside:
                        for kk in range(k + 1, nz):
                            if ct2[i, j, kk] > -1000:
                                break
                        else:
                            outside = True
                    if not outside:
                        ct2[i, j, k] = -999
                        print('Voxel X,Y,Z = (', i, ',', j, ',', k, ') converted '
                              'from HU = -1000 to -999')

    print('Done.')
    return ct2


def convert_to_material(ct, density_bin_size=0.05):
    """ convert HU to material and density
    returns two arrays, specifying material and density
    HU conversion parameters need to be specified below

    bin densities for each material to the central value for each bin
    density_bin_size specifies the width of the bin
    """

    print('\n--------------------------------------------------')
    print('Converting voxels from HU to material and density.')
    print('--------------------------------------------------\n')

    # max_hu is the maximum value in the range of mu to which the material
    # index corresponds
    # mats is the material index
    # dmin and dmax are the minimum and maximum densities for that material

    # EDIT THESE to be what you want!
    max_hu = [-950, -200, 0, 200, 5000]
    mats = [0, 2, 17, 16, 4]
    dmin = [0, .2, 0.85,  .95, 1.85]
    dmax = [0, .7, 0.95, 1.05, 1.9]

    # arrays to hold converted material index and density
    mat_array = np.zeros(ct.shape, dtype=np.uint32)
    den_array = np.zeros(ct.shape)

    for (i, j, k), v in np.ndenumerate(ct):
        if not i % 10 and j == 0 and k == 0:
            print('Now converting X =', i)
        for ind, hu_val in enumerate(max_hu):
            if v < hu_val:
                mat_index = mats[ind]
                deltad = float(dmax[ind] - dmin[ind])
                if mat_index != '0':
                    density = (dmin[ind] +
                        float((v-max_hu[ind-1])) /
                        float((max_hu[ind]-max_hu[ind-1]))
                            * deltad)
                    density = (round(density/density_bin_size) *
                               density_bin_size)
                else:
                    density = 0
                break
        mat_array[i, j, k] = mat_index
        den_array[i, j, k] = density

    return mat_array, den_array


def downsample(ct, rf):
    # ASSUME that each slice has same number of voxels in each direction
    # average over 4 voxels (2x2)
    print('Downsampling CT by a factor of', rf)
    nvox = ct.shape[0] / rf  # number of voxels wanted in the result
    nslices = ct.shape[1]

    newct = np.zeros((nvox, nslices, nvox))

    for s in range(nslices):
        slice1 = ct[:, s, :]
        if not s % 10:

            print('Now processing slice', s)

        for i in range(int(nvox)):
            for j in range(int(nvox)):
                avg = 0.0
                count = 0.0
                for ii in range(rf):
                    for jj in range(rf):
                        avg += float(slice1[i*rf+ii, j*rf+jj])
                        count += 1.
                avg /= count
                newct[i, s, j] = avg

    return newct


def write_out(material, density, sizex=0.0, sizey=0.0, sizez=0.0,
              filename='tmp.phantom'):
    """ write the results to a file
    material and density are numpy arrays with material index and density
    """
    print('\n----------------------')
    print('Writing out data file.')
    print('----------------------\n')

    nx, ny, nz = material.shape

    outst = (
    '# VirtuaLinac phantom definition file\n'
    '# The next 2 lines are required by VirtuaLinac:\n'
    '# Number of voxels: {0} {1} {2}\n'
    '# Voxel size: {3:g} {4:g} {5:g} mm\n'
    .format(nx, ny, nz, sizex, sizey, sizez))

    for (i, j, k), m in np.ndenumerate(material):
        if not i % 10 and j == 0 and k == 0:
            print('Now writing out X =', i)
        if density[i, j, k] == 0:
            d = '0'
        else:
            d = str(density[i, j, k])
        outst += str(m) + ' ' + d + '\n'

    f = open(filename, 'w')
    f.write(outst)
    f.close()
    print('Done.')


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--xmin',
                        help='Minimum X index to keep.',
                        default=-1)
    parser.add_argument('--xmax',
                        help='One more than then maximum X index to keep.',
                        default=-1)
    parser.add_argument('--ymin',
                        help='Minimum Y index to keep.',
                        default=-1)
    parser.add_argument('--ymax',
                        help='One more than the maximum Y index to keep.',
                        default=-1)
    parser.add_argument('--zmin',
                        help='Minimum Z index to keep.',
                        default=-1)
    parser.add_argument('--zmax',
                        help='One more than the maximum Z index to keep.',
                        default=-1)
    parser.add_argument('--densitybinsize',
                        help='Size of density bins',
                        default=0.05)
    parser.add_argument('--filename',
                        help='Name of file to write',
                        default='tmp.phantom')
    parser.add_argument('-d', '--directory',
                        help='Directory of CT data',
                        default='.')
    args = parser.parse_args()

    density_bin_size = float(args.densitybinsize)

    ct_1, sizex, sizey, sizez = read_in(args.directory)

    xmin = 0
    if xmin < int(args.xmin) < ct_1.shape[0]:
        xmin = int(args.xmin)
    xmax = ct_1.shape[0]
    if 0 < int(args.xmax) < xmax:
        xmax = int(args.xmax)
    ymin = 0
    if ymin < int(args.ymin) < ct_1.shape[1]:
        ymin = int(args.ymin)
    ymax = ct_1.shape[1]
    if 0 < int(args.ymax) < ymax:
        ymax = int(args.ymax)
    zmin = 0
    if zmin < int(args.zmin) < ct_1.shape[2]:
        zmin = int(args.zmin)
    zmax = ct_1.shape[2]
    if 0 < int(args.zmax) < zmax:
        zmax = int(args.zmax)

    print(xmin, xmax, ymin, ymax, zmin, zmax)
    # define a resampling factor
    rf = 4
    ct_1 = downsample(ct_1, rf)
    # TODO this should be done by downsample:
    sizex *= rf
    sizez *= rf
    ct_1 = ct_1[xmin:xmax, ymin:ymax, zmin:zmax]
    ct_1 = outside_to_air(ct_1)
    ct_1 = inside_to_lung(ct_1)

    mat, dens = convert_to_material(ct_1, density_bin_size=density_bin_size)

    write_out(mat,
              dens,
              sizex=sizex,
              sizey=sizey,
              sizez=sizez,
              filename=args.filename)


if __name__ == '__main__':
    main()
