=====
Usage
=====

Check how many jobs are running. These are the outputs of various Linux commands: date, uptime, ps . 

Load average is the CPU usage, averaged over the preceding 1, 5, and 15 minutes. Ideally the values will be the same as the number of cores, but may be lower for jobs with a lot of file i/o.

Jobs may be terminated using the 'kill' link in the table.
