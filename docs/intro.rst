============
Introduction
============

Varian's VirtuaLinac is a web front end to our Geant4 simulation code. It contains an accurate and configuragle model of the TrueBeam treatment head, and options to record dose distributions to phantoms or a phase space file for importing into another simulation code. Particles may be recorded on a flat plane in the treatment beam, or on a sphere surrounding the treatment head.

Simulation parameters are specified through a web interface. Input and output files, including phase space files, dose distributions, phantom material files, and Developer Mode trajectory files are stored in one directory. This files will disappear when the instance terminates so make sure to download any needed files.

Dose distributions and phase space files can be plotted online.
