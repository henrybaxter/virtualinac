Physics
=======

Code
----

The only option is Geant4.

Version
-------

Which version of Geant4 to use. In general it will default to the latest Geant4 version available in VirtuaLinac, and it is recommended to stick with this version.

Physics list
------------

Which Geant4 physics list to use. The list is chosen via the Geant4 physics factory methods, in which one can mix and match hadronic physics and electromagnetic physics. 

 * So far, the only hadronic physics used is photonuclear/electronuclear interactions, and every hadronics list used the same model. The only choice implemented is QGSP_BIC. Note that I patch Geant4 such that the photonuclear interaction produces the correct number of neutrons (at least one non-gamma per interaction).
 * Accurate tracking of neutrons below 20 MeV requires the HP data set. This will slow the initialization of the simulation, and is not needed if accurate neutron tracking is required.
 * Electromagnetic physics lists go by two names. The most accurate is G4EmStandardPhysics_option4, also known as EMZ. Other options are G4EmStandardPhysics_option3, also known as EMY. Not implemented here are the Livermore and Penelope lists. 

Range cut
---------

When a secondary particle is created, Geant4 decides whether or not to track it based on its radiation length. If the radiation length is lower than the range cut, the particle is not tracked and its energy is deposited locally. Values in the range 10-100 um are good for standard water tank phantoms, with voxel sizes of several millimeters.

Random number seed
------------------

The random number seed. To generate statistically independent results when re-running with the same parameters, choose a new random number seed. The Mersenne Twister random number generator is used, and the seed can be in the range 1 to 2^63 - 1.

Brem splitting
--------------

For each bremsstrahlung event, create N photons each of weight 1/N. This reduces the time spent tracking electrons in the target. A factor of 100, and 100x fewer incident electrons, reduces simulation time by a factor of 4. Note that the brem splitting is not specific to the target. Also, splitting only happens once: If a brem interaction occurs for a low-weight electron, there is no splitting. This command uses the native Geant4 electromagnetic brem splitting, appearing in the macro file as ::

  /process/em/setSecBiasing eBrem world 100 100 MeV

Photon reuse factor
-------------------

A method of speeding up dose calculations to a virtual phantom. A box surrounds the phantom. Every photon incident on the surface of the box is split into N identical photons, where N is the value entered. The weight of each photon is multiplied by 1/N. This is useful because the chance of a given photon interacting in a voxel is low, so each photon can be reused. A suggested upper limit is 20.

Simulate head shielding
-----------------------

If checked, treatment head shielding away from the treatment beam is shielded. This includes the shielding for backscatter from the target, shielding around the jaws, and the covers. Note that shielding adjacent to the treatment beam (e.g. primary collimator, and shielding between primary collimator and jaws) is always shielded. This will make simulation times longer.

