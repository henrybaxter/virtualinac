=========
Electrons
=========

Specify components for simulation of electron beams.

I.C. metal thickness factor
---------------------------

Thickness of the metal forming the electrodes of the ion chamber (monitor chamber). The profiles of the lower energy beams in particular are sensitive to this parameter. A value of 0.8 was used for the electron phase space files on MyVarian.com.

Foil 1
------

Primary scattering foil. Choose one of the existing ones, none, or create your own (custom). For custom, you will be given the choice of material, thickness, radius, and position offset. The latter is relative to the nominal position of the existing foils.

Thickness factor
----------------

Set the thickness of the primary foil to be the nominal thickness multiplied by this factor. Values in the region 1.05 to 1.10 were used to create the phase space files on myVarian.com, depending on which beam.

Foil 2
------

The secondary scattering foil. As for foil 1, you may choose one of the existing ones, none, or create your own (custom). The custom foils are not stepped.
