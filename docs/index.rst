.. firsttest documentation master file, created by
   sphinx-quickstart on Wed Jul 29 16:11:47 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _contents:

VirtuaLinac documentation contents
==================================

.. toctree::
   :maxdepth: 2

   intro
   beamtype
   physics
   xrays
   electrons
   incident
   output
   beamshaping
   plotting
   usage
   api_tutorial
   api_reference

Indices and tables
==================

* :ref:`search`

