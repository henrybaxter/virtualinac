============
API tutorial
============

The VirtuaLinac can be accessed programmaticly, through the API. There are really two APIs: one is to communicate with Amazon Web Services, and the other to communicate with VirtuaLinac.

A typical workflow might be to start an instance, get the IP address of that instances, upload a file, run a simulation with given parameters, check to see when the simulation is done, download the results files, then terminate the instance. 

I write my scripts in Python so I'll give examples in Python. It should be possible to write these scripts in different languages. Also, rather than go over installing external packages in depth, I'll provide links to their documentation. Furthermore details of filenames, directory structure etc. are Linux-centric. It can be done in Windows too.

Also note the examples below are using Python2, with print_function() imported from Python3::

  from __future__ import print_function()



Amazon Web Services API
-----------------------

The Python package boto3 is very useful. Documentation is here:

https://github.com/boto/boto3

(scroll down to see the documentation). First you will need to install boto3::

  pip install boto3

Then set up your AWS credentials in ~/.aws/credentials::

  [default]
  aws_access_key_id = YOUR_ACCESS_KEY
  aws_secret_access_key = YOUR_SECRET_KEY

and region in ~/.aws/config::

  [default]
  region=us-east-1

US-east-1 is equivalent to N. Virginia on the AWS console page.

Create an instance
^^^^^^^^^^^^^^^^^^

For now, create an instances (or multiple instances) throught the AWS web interface.

Get the instances and IP address
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First we can get the instances, then iterate over the data structure to find information about each instance::

  >>> import boto3
  >>> ec2=boto3.resource('ec2')
  >>> instances = ec2.instances.filter(Filters=
      [{'Name': 'instance-state-name', 'Values': ['running'] }])
  >>> instances
  ec2.instancesCollection(ec2.ServiceResource(), ec2.Instance)
  >>> for instance in instances:
          print(instance.instance_id, instance.public_dns_name)
     
  i-fdcXXXXX ec2-XXX-XXX-XXX-XXX.compute-1.amazonaws.com

Let's make a note of the URL:::

  >>> dns_names = []
  >>> for instance in instances:
          dns_names.append(instance.public_dns_name)
  >>> url = 'http://' + dns_name[0]

Terminate the instance
^^^^^^^^^^^^^^^^^^^^^^

Assume we know which instance to terminate (e.g. by looping over the instances found above). In this example there is only one instance, so we can terminate the first (and only) one::

  >>> instances[0].terminate()

VirtuaLinac API
---------------

VirtuaLinac is a web application, and communication to it is through http. This can be done either through the browswer, or programmatically through the REST (representational state transfer) API.

There are two Python packages I use to interact with instances through the API. One is requests. As the name suggests, this is used to send requests to a web site and receive information. 

http://docs.python-requests.org/en/master/ ::

  >>> import requests

The other is BeautifulSoup, useful for scraping information from an html page:

https://www.crummy.com/software/BeautifulSoup/ ::

  >>> from bs4 import BeautifulSoup

Get instance usage
^^^^^^^^^^^^^^^^^^

Let's see if any jobs are running on the instance. This is on the Usage tab of the web interface. Get this using requests, then examine the text returned. Look for the line with "Number of jobs".::

  >>> r = requests.get(url + '/usage')
  >>> for line in r.text.split('\n'):
          if 'Number of jobs' in line:
              print(line)
  <p><b>Number of jobs:</b> 0 </p>

This page is relatively straightforward, so we can parse it without using BeautifulSoup.::

  >>> jobs = -1
  >>> for line in r.text.split('\n'):
          if 'Number of jobs' in line:
                  jobs = int(line.split()[-2])
     
  >>> jobs
  0

Jobs on VirtuaLinac are run in parallel, so it is best to wait until there are no jobs running before submitting one. Now there are none so we can submit a job.

Submit a job
^^^^^^^^^^^^

This involves making a REST POST request, using requests. The data to post is the data that would be filled out in the VirtuaLinac web form. In Python we can create a dictionary that has the data. For example: ::

  >>> params = {
      'filename'     : 'api_test_1',
      'code_version' : 0,
      'physics_list' : 0,
      'beam_type'    : 0,
      'range_cut' : 10,
      'energy'       : 6.84,
      'energy_spread' : 0.6,
      'spot_size_x'  : 0.7,
      'spot_size_y'  : 0.7,
      'beam_divergence_x' : 0.0,
      'beam_divergence_y' : 0.0,
      'jaw_position_y1' : -20,
      'jaw_position_y2' :  20,
      'jaw_position_x1' : -20,
      'jaw_position_x2' :  20,
      'phantom_bool' : 'True',
      'phantom_size_x' : 500,
      'phantom_size_y' : 500,
      'phantom_size_z' : 400,
      'phantom_voxels_x' : 125,
      'phantom_voxels_y' : 125,
      'phantom_voxels_z' : 100,
      'phantom_position_x' : 0,
      'phantom_position_y' : 0,
      'phantom_position_z' : -5,
      'incident_particles' : 10000,
      'brem_splitting' : None,
      'splitting_factor' : None,
      'phsp_record'  : None
  }

then submit it: ::

  >>> r = requests.post(url, params=params)

You might want to look at r.text to make sure the job was submitted correctly. 

There is no good error reporting with the API. If r.text contains the macro (as in the web interface), the job was successfully submitted. If the job runs, it was with the parameters in the macro file.

Details of the parameters are given below. 

Some points:

* The value is passed to VirtuaLinac as a string. It will be converted to one if it is not already.
* Many VirtuaLinac parameters have default values and can be omitted. If doing so be careful to check that they are set to what you expect.
* Checkboxes in the web interface expect a boolean value. Strings will convert to True. Thus, to set a checkbox to the unchecked state, set the parameter value to None.
* The default for any checkbox is always False.
* Dropdown boxes have items that are a key/value pair. The web interface displays the value, but for the API it is necessary to specify the key. For some of the dropdown boxes, the keys are integers. Consult the API reference to determine the keys.

Determining keys for the dropdown boxes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This section can be skipped. The best way to get the keys is from the documentation. An alternative method is presented here, because it is a useful method for other applications.

Specifying values for the dropdown boxes can be tricky, because the option value isn't always the text that appears in the web interface. (I would like to change this.) For most of the dropdown boxes, there is an integer corresponding to the selection. For example, physics_list 0 corresponds to QGSP_BIC.

To get the value required, use BeautifulSoup to parse the form. The dropdown boxes have html tag 'select'.::

  >>> resp = requests.get(url)
  >>> soup = BeautifulSoup(resp.text, 'html.parser')
  >>> links = soup.find_all('select')
  >>> links[3] ## for example
  <select id="physics_list" name="physics_list"><option value="0">QGSP_BIC</option>
  <option selected="" value="1">QGSP_BIC_EMZ</option><option value="2">QGSP_BIC_HP_EMY</option>
  <option value="3">QGSP_BIC_HP_EMZ</option></select>

In practice a good way of doing this is to create a dictionary of dictionaries, with all the options:::

  >>> options = {}
  >>> for link in links:
          result = {}
          name = link['id']
          opts = link.find_all('option')
          for opt in opts:
                  val = opt['value']
                  txt = opt.string
                  result[txt] = val
          options[name] = result

Then we can set the parameters by the desired string:::

  >>> parameters['physics_list'] = options['physics_list']['QGSP_BIC_EMZ']


Specifying an input file
^^^^^^^^^^^^^^^^^^^^^^^^

Various dropdown boxes are automatically populated with filenames found the the 'File Storage' area. In order to specify these for the input parameters follow this example.

The list of files is at '/phspfiles'. 

As for dropdown boxes in general, the challenge is getting the value corresponding to the known string (here the string is the filename). The key is that the filenames are sorted alphanumerically in both the list of files, and the dropdown box. Thus, for example, the third phase space file will have index 2 (the index is zero-based). The sorting is done by Python sort(). I realize this might be hard to reproduce in other languages, especially if non-alphanumeric characters are used. If you run into difficulties let me know.

One trick is that most files have two links or more links. There is the link to the file, plus links to plot and to delete. Be sure to choose the correct link: it is the one with 'vl_files' in it.

Note for input phase space files, the header must be present.

Suppose we want to specify the input phase space file to be 'my_phsp.phsp'. We'll get the index with BeautifulSoup:::

  >>> inputfile = 'my_phsp.phsp'
  >>> files = []
  >>> resp = requests.get(url + '/phspfiles')
  >>> soup = BeautifulSoup(resp.text, 'html.parser')
  >>> links = soup.find_all('a')
  >>> for link in links:
          href = link['href']
          if 'vl_files' in href:
            spl = href.split('/')
            if len(spl) > 2:
                filename = href.split('/')[2]
                if filename.split('.')[-1] == 'phsp':   ## verify the suffix
                    files.append(filename)
  >>> files.sort()
  >>> index = -1
  >>> for i, f in enumerate(files):
          if f == inputfile:
              index = i
  >>> if index >= 0:
          print('Found file', inputfile, 'at index', index)


Then set the parameters appropriately:::

  >>> params['phsp_input_file'] = index

Uploading input files
^^^^^^^^^^^^^^^^^^^^^

If you want to upload a file, for example named my_phsp.phsp, use requests to POST it to the /upload page:::

  >>> filename = 'my_phsp.phsp'
  >>> files = {'file': open(filename, 'rb')}
  >>> r = requests.post(url + '/upload', files=files)

Don't forget to upload the header as well!

Downloading results
^^^^^^^^^^^^^^^^^^^

The list of files is at '/phspfiles', but the files themselves are in the directory '/vl_files'. If you know the file name, uses requests to get it. It may be a large file so try to stream it.::

  >>> filename = 'my_phsp.phsp'
  >>> resp = requests.get(url + '/vl_files/' + filename, stream=True)

Then, save it. Similarly it might be a large file so write in parts. (As elsewhere, I'm skipping over various checks. For example, you might want to check to see if the file already exists before overwriting it.):::

  >>> with open(filename, 'wb') as f:
          for chunk in resp.iter_content():
                  f.write(chunk)
 
Creating a plot
^^^^^^^^^^^^^^^

The plotting functions are really meant to be a quick visual test, with the expectation that serious data analysis would take place once the dose and phase space files are downloaded. Nevertheless, there's no reason not to create and download plots. 

The steps are first to generate them, then to download them. The plot parameters are submitted to VirtuaLinac as a POST request, then internally VirtuaLinac creates the plot with a GET request. We can skip right to the GET request. First create a dictionary with plot parameters.::

  >>> plotparams = {}
  >>> plotparams['colormap'] = 'True'
  >>> plotparams['particle'] = 1
  >>> plotparams['yaxis'] = 2
  >>> plotparams['abscissa'] = 1
  >>> plotparams['ymin'] = -40
  >>> plotparams['ymax'] = 40
  >>> plotparams['xmin'] = -40
  >>> plotparams['xmax'] = 40

  >>> plot_url = url + '/figphsp/' + filename + '.phsp'
  >>> extension = '.2d.phsp.png'
  >>> r = requests.get(plot_url, params=plotparams, stream=True)
  >>>  with open(filename + extension, 'wb') as f:
           for chunk in r.iter_content():
               f.write(chunk)

For plotting dose distributions, the directory is different, and the parameters are different: ::

    >>> doseparams = {}
    >>> doseparams['direction'] = 'z'
    >>> doseparams['average'] = 5
    >>> doseparams['colormap'] = 'True'
    >>> extension = '2d.z.png'
    >>> plot_url = url + '/fig/' + filename + '.dose'
    >>> extension = '.z_compare.png'
    >>> r = requests.get(plot_url, params=doseparams, stream=True)
    >>>  with open(filename + extension, 'wb') as f:
             for chunk in r.iter_content():
                 f.write(chunk)

It is also possible to get the plot data as a text file. See the API reference for details.

Putting it all together
-----------------------

A complete program that partially automates the VirtuaLinac job control is available at:

https://bitbucket.org/darens/virtualinac

Recall that this uses boto2 rather than boto3 as above. It is similar enough, but there is more overhead.

