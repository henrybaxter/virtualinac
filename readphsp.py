#!/usr/bin/env python
"""Read a binary phase space file and print ascii to
the screen. Optionally bin the data and plot it.
"""

# Copyright (c) 2015-2016, Varian Medical Systems
# All rights reserved.
#
# Released under terms of the Varian Open Source license, available at
# http://radiotherapyresearchtools.com/license
# which states, among other points, the code is not for clinical use,
# and there is no warranty.
#
# Author:  Daren Sawkey <daren dot sawkey at varian dot com>
# January 22, 2016

from __future__ import print_function
import os,sys
import struct
import argparse
import math
import time
import numpy as np

def _normalize(x, y, yerr=None, dir=None):
    """Rescale the plot such that values near x=0
     are 100%
    """
    if dir != 'z': ## always true for phsp
        #sum = 0.
        #count = 0
        ## find the two points either side of x=0
        ## xvalues are in different units for phsp/dose
        found = False
        xtmp1 = -999
        xtmp2 =  999
        ytmp1 = 0
        ytmp2 = 0
        for xval, yval in zip(x,y):
            if 0 > xval > xtmp1:
                xtmp1 = xval
                ytmp1 = yval
            if 0 < xval < xtmp2:
                xtmp2 = xval
                ytmp2 = yval
        avg = (ytmp1 + ytmp2)/2.
        y /= (avg/100.)
        #yerr /= (avg/100.)

def _get_percent_diffs(x1,y1,x2,y2):
    ## find the % differences
    # for each mc point, find all msmt points for which the closest mc point
    # is the one under consideration
    # exception is at the last mc point; limit to 2 mm deeper
    """ x1/y1 assumed to be sim (fewer data points) """
    # do the first and last points separately, to limit the range
    xdiffs = []
    ydiffs = []
    for i in range(len(x1)):
        diff = 0
        count = 0
        for j in range(len(x2)):
            xdelta = abs(x2[j] - x1[i])
            use_this_point = True
            if (i == 0) or (i == len(x1) - 1):
                if abs(x2[j] - x1[i]) > 3:
                    use_this_point = False
            for k in range(len(x1)):
                if abs(x2[j] - x1[k]) < xdelta:
                    use_this_point = False
                    break
            if use_this_point:
                diff += y1[i] - y2[j]
                count += 1
        if count > 0:
            ydiffs.append(float(diff)/float(count))
            xdiffs.append(x1[i])
        else:
            pass
    return xdiffs, ydiffs



def read_header(filename, 
                verbosity=0):
    fn = ''
    if filename[-9:] == '.IAEAphsp':
        filename = filename[:-9]
    elif filename[-5:] == '.phsp':
        filename = filename[:-5]
    print(filename)
    header = {}
    beamnrc_format = False
    if os.path.exists(filename + '.header'):
        fn = filename + '.header'
    elif os.path.exists(filename + '.IAEAheader'):
        fn = filename + '.IAEAheader'
        beamnrc_format = True
    else:
        ## if there is no header, assume defaults
        ## alternatively could exit, but this allows us to read
        ## without header
        ## TODO allow to pass what is stored on command line
        header['x_stored']  = 1
        header['y_stored']  = 1
        header['z_stored']  = 1
        header['u_stored']  = 1
        header['v_stored']  = 1
        header['w_stored']  = 0
        header['wt_stored'] = 1
        header['extra_float_stored'] = 2
        header['extra_long_stored']  = 0
        header['format'] = 'bfffffffii'
        header['$RECORD_LENGTH:'] = 37
        return header

    ## in case it isn't either stored or constant in header
    header['wt_constant'] = 1

    with open(fn, 'r') as f:
        ll = f.readlines()
    for i, l in enumerate(ll):
        key = '$RECORD_LENGTH:'
        if l[:len(key)] == key:
            header[key] = int(ll[i+1])
        key = '$RECORD_CONTENTS:'
        if l[:len(key)] == key:
            ## assume fixed format
            ## TODO better to read lines
            header['x_stored']  = int(ll[i+1].split()[0])
            header['y_stored']  = int(ll[i+2].split()[0])
            header['z_stored']  = int(ll[i+3].split()[0])
            header['u_stored']  = int(ll[i+4].split()[0])
            header['v_stored']  = int(ll[i+5].split()[0])
            if beamnrc_format:
                header['w_stored']  = 0
            else:
                header['w_stored']  = int(ll[i+6].split()[0])
            header['wt_stored'] = int(ll[i+7].split()[0])
            header['extra_float_stored'] = int(ll[i+8].split()[0])
            header['extra_long_stored']  = int(ll[i+9].split()[0])
            
            fmt = 'bf'  ## type and energy always recorded
            if header['x_stored'] == 1:
                fmt += 'f'
            if header['y_stored'] == 1:
                fmt += 'f'
            if header['z_stored'] == 1:
                fmt += 'f'
            if header['u_stored'] == 1:
                fmt += 'f'
            if header['v_stored'] == 1:
                fmt += 'f'
            if header['w_stored'] == 1:
                fmt += 'f'
            if header['wt_stored'] == 1:
                fmt += 'f'
            for i in range(header['extra_float_stored']):
                fmt += 'f'
            for i in range(header['extra_long_stored']):
                fmt += 'i'
            header['format'] = fmt
        ## TODO how to check for a general constant value?
        ## now just look for constant Z or constant weight
        key = '$RECORD_CONSTANT:'
        if l[:len(key)] == key:
            j = i + 1
            const_lines = []
            while ll[j].strip() != '':
                const_lines.append(ll[j])
                j += 1
            for k, l1 in enumerate(const_lines):
                if 'constant z' in l1.lower():
                    header['z_constant'] = float(l1.split()[0])
                if 'constant weight' in l1.lower():
                    header['wt_constant'] = float(l1.split()[0])
    if verbosity > 1:
        print(header)
    return header

def readphsp(filename, 
             #header, 
             binparams,         # dictionary containing binning parameter
             printcount=10000, 
             verbosity=0, 
             popup=False,       # if true, plot appears in separate window.
             savefig=False,     # save a png of plot image             
             export=False):

    header = read_header(filename, verbosity=verbosity)
   
    # set up for binning
    bin_data  = binparams['bin']  # bool, whether or not to bin data and plot
    xmin      = binparams['xmin']
    xmax      = binparams['xmax']
    xnbins    = binparams['xnbins']
    particle  = binparams['particle']
    xaxis     = binparams['xaxis']
    condition = binparams['condition']
    fluence   = binparams['fluence']
    multiply  = binparams['multiply']
    colormap = False
    if 'colormap' in binparams.keys():
        colormap  = binparams['colormap']
    if 'cmap' in binparams.keys():
        cmap = binparams['cmap']
    else:
        cmap = 'viridis'
    if 'aspect' in binparams.keys():
        aspect = binparams['aspect']
        if aspect.lower() != 'equal' and aspect.lower() != 'auto':
            try:
                aspect = float(aspect)
            except:
                aspect = 'auto'
    else:
        aspect = 'auto'
    msmt_file = None
    if not colormap:
        if 'msmt_file' in binparams.keys():
            msmt_file = binparams['msmt_file']
    yaxis = binparams['yaxis'] if 'yaxis' in binparams.keys() else 'y'
    ymin = binparams['ymin'] if 'ymin' in binparams.keys() else -30
    ymax = binparams['ymax'] if 'ymax' in binparams.keys() else 30
    ylog = binparams['ylog'] if 'ylog' in binparams.keys() else False
    ynbins = binparams['ynbins'] if 'ynbins' in binparams.keys() else 100
    ybinsize   = (ymax - ymin) / float(ynbins)
    xbinsize   = (xmax - xmin) / float(xnbins)
    xbin = []
    ybin = []

    if ylog and ymin <= 0.0:
            ymin = 0.001

    if colormap:
        film = np.zeros([xnbins, ynbins])
    else:
        for i in range(xnbins):
            xbin.append(xmin + (i+0.5) * xbinsize)
            ybin.append(0.)

    if export:
        fn_out = filename + '.conv'
        f_output = open(fn_out,'wb')

    ## number of bytes for each particle
    recordlength = header['$RECORD_LENGTH:']
    #fn = os.path.join(filename, '.phsp')
    if (os.path.exists(filename)):
        f = open(filename, 'rb')
    elif (os.path.exists(filename + '.phsp')):
        f = open(filename + '.phsp', 'rb')
    elif (os.path.exists(filename + '.IAEAphsp')):
        f = open(filename + '.IAEAphsp', 'rb')
    else:
        print('Phase space file', filename, 'not found!')
        return

    ## number of particles to read at once
    ## supposed to save time by reducing number of calls to unpack
    number_to_read = 100000
    count = 0

    l = 'starting'
    while (l):
        l = f.read(number_to_read * recordlength)
        ## actual number read; will be lower at end of file
        number_read = int(len(l)/recordlength)
        fmt = '='
        fmt1 = header['format']
        for i in range(number_read):
            fmt += fmt1

        num_vals = len(fmt1)
        s = struct.unpack(fmt,l)
        num_extra_longs = header['extra_long_stored']
        num_extra_floats = header['extra_float_stored']
        for j in range(number_read):
            count += 1
            i = 0
            ty = s[i+num_vals*j]
            i += 1
            en = s[i+num_vals*j]
            new_part = en < 0
            en = abs(en)
            i += 1
            x  = s[i+num_vals*j]
            i += 1
            y  = s[i+num_vals*j]
            i += 1
            if header['z_stored']:
                z  = s[i+num_vals*j]
                i += 1
            else:
                z = header['z_constant']            
            u = s[i+num_vals*j]
            i += 1
            v = s[i+num_vals*j]
            i += 1
            sign_w = 1
            if header['wt_stored']:
                wt = s[i+num_vals*j]
                if wt < 0:
                    sign_w = -1
                    wt *= -1
                i += 1
            else:
                wt = header['wt_constant']            
            extra_longs = []
            extra_floats = []
            if num_extra_floats:
                for k in range(num_extra_floats):
                    extra_floats.append(float(s[i+num_vals*j]))
                    i += 1
            if num_extra_longs:
                for k in range(num_extra_longs):
                    extra_longs.append(int(s[i+num_vals*j]))
                    i += 1
           
            # print data if requested
            if verbosity > 1:
                if count < printcount:
                    if eval(condition):
                        print('{0:9.9g} {1:g} {2:12.7} {3:8.4f} {4:8.4f} '
                              '{5:8.4f} {6:8.4f} {7:8.4f} {8:8.4f}'
                            .format(count,ty,en,x,y,z,u,v,wt), end='')
                        for j in range(len(extra_longs)):
                            print(' {0:8g}'.format(extra_longs[j]), end='') 
                        for j in range(len(extra_floats)):
                            print(' {0:12.7g}'.format(extra_floats[j]), end='')
                        print('')

            ## bin the data
            if bin_data:
                if ty == particle or particle == 0: # 0 == all
                    #print('In bin_data, yaxis:', yaxis)
                    if xaxis == 'en':
                        xval = en
                    elif xaxis == 'x':
                        xval = x
                    elif xaxis == 'y':
                        xval = y
                    elif xaxis == 'r':
                        xval = math.sqrt(x*x+y*y)
                    else:
                        xval = eval(xaxis)
                    if colormap:
                        if yaxis == 'en':
                            yval = en
                        elif yaxis == 'x':
                            yval = x
                        elif yaxis == 'y':
                            yval = y
                        #elif yaxis == 'r':
                        #    yval = math.sqrt(x*x+y*y)
                        else:
                            yval = eval(yaxis)

                    doit = False
                    if condition == '1':  # don't eval('1')
                        doit = True
                    elif eval(condition):
                        doit = True
                    if doit:
                        bin_x = int(math.floor((xval - xmin)/xbinsize))
                        if colormap:
                            bin_y = int(math.floor((yval - ymin)/ybinsize))
                            #print(bin_y)
                        if (bin_x >= 0 and bin_x < xnbins):
                            ## minimize the time-consuming calls to eval
                            if multiply != '1':
                                mult = eval(multiply)
                            else:
                                mult = 1.
                            value_to_add = mult * wt
                            if fluence:
                                pperp2 = u**2 + v**2
                                if pperp2 < 1.0:
                                    fluencefactor = math.sqrt(1.-pperp2)
                                    value_to_add /= fluencefactor
                                else:
                                    value_to_add = 0.0
                            if xaxis == 'r':
                                xtmp1 = bin_x * xbinsize + xmin
                                area = math.pi * (2 * xbinsize * xtmp1 + 
                                                  xbinsize * xbinsize)
                                value_to_add /= area
                            if colormap:
                                ## have already checked x above
                                if (bin_y >= 0 and bin_y < ynbins):
                                    film[bin_x, bin_y] += value_to_add
                            else:
                                ybin[bin_x] += value_to_add
            ## export a new phsp file
            if export:  
                if (extra_longs[1] == 80):
                    st = struct.pack('=bfffffffii', ty, en, x, y, z, u, v, wt,
                                               extra_longs[0], extra_longs[1])
                    f_output.write(st)

        if verbosity > 0:
            if count % 100000 == 0:
                print('Read particle number:', count/1e6, 'M')
    f.close()

    if export:
        f_output.close()
    
    #print('# of particles binned * multiplication factor * weight:', sum(ybin))

    out = '#binned data from ' + filename + '\n'
    for i in range(len(ybin)):
        yval = ybin[i]
        out += str(xbin[i]) + ' ' + str(yval) + '\n'

    fnout = filename + '.binned'
    if os.path.exists(fnout):
        fnout += '.' + str(int(time.time()))
    f = open(fnout, 'w')
    f.write(out)
    f.close()
   
    if bin_data:
        ## for saving svg need to use this
        #matplotlib.use('Agg')
        import matplotlib.pyplot as plt
        #from matplotlib.figure import Figure

        fig = plt.figure()
        ax = fig.add_subplot(111)
        if colormap:
            if cmap:
                cmap = plt.get_cmap(cmap)
            else:
                cmap = plt.get_cmap('viridis')
            label = 'Counts per bin'
            cax = ax.imshow(
                    film.T, 
                    origin='lower', 
                    interpolation='None',
                    extent=[xmin, xmin+xbinsize*xnbins, 
                            ymin, ymin+ybinsize*ynbins],
                    aspect=aspect,
                    cmap=cmap)
            fig.colorbar(cax,label=label)
        else:
            xbin = np.array(xbin)
            ybin = np.array(ybin)
            if msmt_file:
                _normalize(xbin, ybin)
            if ylog:
                ax.set_yscale('log')
                ax.set_ylim(ymin=ymin, ymax=2.*max(ybin))
            ax.plot(xbin, ybin, 'bo-', label='Simulation')
        if xaxis == 'en':
            xlabel = 'Energy [MeV]'
        elif xaxis == 'x' or xaxis == 'y':
            xlabel = 'Off-axis (' + xaxis + ') position [cm]'
        else:
            xlabel = 'X value: ' + xaxis
        ax.set_xlabel(xlabel)
        if not colormap:
            if xaxis == 'r':
                ax.set_ylabel('Counts per bin/area [cm^-2]')
            else:
                if not msmt_file:
                    ax.set_ylabel('Counts per bin')
                else:
                    ax.set_ylabel('Counts per bin (normalized)')
        else: # colormap
            if yaxis == 'x' or yaxis == 'y': 
                ylabel = 'Off-axis (' + yaxis + ') position [cm]'
            elif yaxis == 'en':
                ylabel = 'Energy [MeV]'
            else:
                ylabel = 'Y value: ' + yaxis
            ax.set_ylabel(ylabel)
        fn = os.path.split(filename)[-1] ## remove the /var/www ...
        ax.set_title('Binned phase space ' + fn)
        
        lines, labels = ax.get_legend_handles_labels()

        ## read and plot data to compare to
        if msmt_file is not None:
            with open(msmt_file, 'r') as f:
                ll = f.readlines()
                xmsmt = []
                ymsmt = []
                for l in ll:
                    if l[0] != '#':
                        lsplit = l.split()
                        xmsmt.append(float(lsplit[0]))
                        ymsmt.append(float(lsplit[1]))
            ax.plot(xmsmt, ymsmt, 'rx-', label='Measurement')
            ax.legend(loc=4, 
                      prop={'size':8},
                      numpoints=1)
            xdiffs, ydiffs = _get_percent_diffs(xbin, ybin, xmsmt, ymsmt)
            ax2 = ax.twinx()
            ax2.plot(xdiffs, ydiffs, 'g+-', label='Difference')
            ax2.set_ylabel('Difference [%]')

            lines2, labels2 = ax2.get_legend_handles_labels()
            loc = 1
            ax2.legend(lines + lines2,
                       labels + labels2,
                       loc=loc,
                       prop={'size':8},
                       numpoints=1)



        #ax.set_yscale('log')
        if popup:
            plt.show()
            return
        else:
            if savefig:
                plt.savefig('test.png')
            return fig

def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('filename', 
                help='Filename')
    parser.add_argument('--verbosity', 
                help='how verbose to be',
                default=1)
    parser.add_argument('--bin',
                help='bin data and plot',
                action='store_true')
    parser.add_argument('--popup',
                help='show resulting graph in new window, rather than '
                'returning a figure (needs --bin).',
                action='store_true')
    parser.add_argument('--xmin',
                help='minimum abscissa value for plot. Default = 0',
                default='0')
    parser.add_argument('--xmax',
                help='maximum abscissa value for plot. Default = 10',
                default='10')
    parser.add_argument('--ymin',
                help='minimum y-value for colormap. Default = -20',
                default='-20')
    parser.add_argument('--ymax',
                help='maximum y-value for colormap. Default = 20',
                default='20')
    parser.add_argument('--particle',
                help='particle to plot. all=0, gamma=1, electron=2, '
                'positron=3, neutron=5. Default = 1',
                default='1')
    parser.add_argument('--xnbins',
                help='number of bins along x axis for plot',
                default='100')
    parser.add_argument('--ynbins',
                help='number of bins along y axis for colormap plot',
                default='100')
    parser.add_argument('--xaxis',
                help='X axis to plot. '
                'Choices are en, x, y, z, u, v, wt, r (= radial distance)',
                default='en')
    parser.add_argument('--yaxis',
                help='Y axis to plot. Only valid if --colormap is chosen.'
                'Choices are en, x, y, z, u, v, wt',
                default='y')
    parser.add_argument('--ylog',
                help='Use log scale for y axis.',
                action='store_true')
    parser.add_argument('--condition',
                help='Only bin particle if this evaluates to True',
                default='1')
    parser.add_argument('--multiply',
                help='Multiply weight of each particle by this factor',
                default='1')
    parser.add_argument('--fluence',
                help='Calculate fluence.',
                action='store_true')
    parser.add_argument('--printcount',
                help='Only print the first N particles',
                default='0')
    parser.add_argument('--colormap',
                help='plot a 2D colormap',
                action='store_true')
    parser.add_argument('--cmap',
                help='Name of matplotlib cmap to use for 3D plots.',
                default='viridis')
    parser.add_argument('--aspect',
                help='aspect ratio for plot (passed to matplotlib). '
                'Default = auto.',
                default='auto')
    parser.add_argument('--measurement_file',
                help='Name of file comtaining measured data to compare to.')
    parser.add_argument('--savefig',
                action='store_true',
                help='Save a png file of the plot.')
    parser.add_argument('--export',
                action='store_true',
                help='write out a new (possibly modified) phase space file')

    args = parser.parse_args()
    binparams = {
        'bin'       : args.bin,
        'particle'  : int(args.particle),
        'xmin'      : float(args.xmin),
        'xmax'      : float(args.xmax),
        'ymin'      : float(args.ymin),
        'ymax'      : float(args.ymax),
        'xnbins'    : int(args.xnbins),
        'ynbins'    : int(args.ynbins),
        'xaxis'     : args.xaxis,
        'yaxis'     : args.yaxis,
        'ylog'      : args.ylog,
        'condition' : args.condition,
        'multiply'  : args.multiply,
        'fluence'   : args.fluence,
        'colormap'  : args.colormap,
        'cmap'      : args.cmap,
        'aspect'    : args.aspect,
        'msmt_file' : args.measurement_file
    }

    fn = args.filename
    verbosity = int(args.verbosity)
    printcount = int(args.printcount)
    popup = args.popup

    #header = read_header(fn, verbosity=verbosity)
    readphsp(fn,
             #header,
             binparams=binparams,
             printcount=printcount,
             verbosity=verbosity,
             popup=popup,
             savefig=args.savefig,
             export=args.export)

if __name__ == '__main__':
    main()
